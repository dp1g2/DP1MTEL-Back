package com.dp1mtel.backend.responses;

import java.util.ArrayList;
import java.util.List;

public class MassiveStorageResponse {
    private String correctlyInserted;
    private String badlyInserted;
    private List<Integer> permissions;

    public MassiveStorageResponse(String correctlyInserted, String badlyInserted, List<Integer> permissions) {
        this.correctlyInserted = correctlyInserted;
        this.badlyInserted = badlyInserted;
        this.permissions = new ArrayList<>();
        this.permissions = permissions;
    }

    public MassiveStorageResponse() {
        this.permissions = new ArrayList<>();
    }

    public String getCorrectlyInserted() {
        return correctlyInserted;
    }

    public void setCorrectlyInserted(String correctlyInserted) {
        this.correctlyInserted = correctlyInserted;
    }

    public String getBadlyInserted() {
        return badlyInserted;
    }

    public void setBadlyInserted(String badlyInserted) {
        this.badlyInserted = badlyInserted;
    }

    public List<Integer> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Integer> permissions) {
        this.permissions = permissions;
    }
}
