package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.ProductItem;
import com.dp1mtel.backend.model.ProductItemId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductItemRepository extends JpaRepository<ProductItem,ProductItemId> {
    List<ProductItem> findByPrimaryKey_Product_Id(String productId);
    List<ProductItem> findByPrimaryKey_Item_Id_AndPrimaryKey_Product_ActiveTrue(String itemId);
}
