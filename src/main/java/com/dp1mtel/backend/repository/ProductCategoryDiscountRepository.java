package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.ProductCategoryDiscount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductCategoryDiscountRepository extends JpaRepository<ProductCategoryDiscount, Long> {
    List<ProductCategoryDiscount> findByProductCategory_Id(String prodCatDiscId);
}
