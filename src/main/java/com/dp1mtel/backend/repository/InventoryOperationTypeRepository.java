package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.InventoryOperationType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InventoryOperationTypeRepository extends JpaRepository<InventoryOperationType,Long> {
    List<InventoryOperationType> findByNameEquals(String name);
}
