package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.OrderStatus;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderStatusRepository extends JpaRepository<OrderStatus, Long> {
    Optional<OrderStatus> findBySlugEquals(String slug);
}
