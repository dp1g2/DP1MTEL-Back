package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.GeneralInformation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GeneralInformationRepository extends JpaRepository<GeneralInformation,Long> {
}
