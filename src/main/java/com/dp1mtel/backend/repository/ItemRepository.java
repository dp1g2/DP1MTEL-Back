package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, String> {
    List<Item> findByNameEquals(String name);
}
