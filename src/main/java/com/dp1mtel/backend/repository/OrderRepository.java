package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByStore_IdAndOrderStatus_SlugEquals(Long storeId, String orderStatusSlug);
    List<Order> findByOrderStatus_Slug(String orderStatusSlug);
    List<Order> findAllByIdIn(List<Long> idList);
}
