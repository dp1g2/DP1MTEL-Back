package com.dp1mtel.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.dp1mtel.backend.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByStoreId(Long storeId);
    Optional<User> findByDni(String dni);
    Optional<User> findByPasswordAndDni(String password, String dni);
    Optional<User> findByEmail(String email);
    Optional<User> findByResetToken(String resetToken);
}
