package com.dp1mtel.backend.repository


import com.dp1mtel.backend.model.UserAddress
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface AddressRepository : JpaRepository<UserAddress, Long> {

    @Query("SELECT * FROM user_address A INNER JOIN customer_addresses CA ON CA.addresses_id = A.id WHERE CA.customer_id = (SELECT id FROM customer WHERE document = ?1)",nativeQuery = true)
    fun findByCustomer_DocumentEquals(document: String): List<UserAddress>
}