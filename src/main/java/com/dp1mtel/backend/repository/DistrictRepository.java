package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.District;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistrictRepository extends JpaRepository<District,Long> {
}
