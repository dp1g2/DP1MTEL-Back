package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Permission;
import com.dp1mtel.backend.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PermissionRepository extends JpaRepository<Permission, Long> {
    Optional<Permission> findByNameEquals(String name);
}
