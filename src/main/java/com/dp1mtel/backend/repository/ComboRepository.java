package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Combo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComboRepository extends JpaRepository<Combo,String> {
}
