package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;


public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByPasswordAndPhone(String password, String phone);
    Optional<Customer> findByDocument(String document);
    Optional<Customer> findByResetToken(String resetToken);
    Optional<Customer> findByPhone(String phone);
}
