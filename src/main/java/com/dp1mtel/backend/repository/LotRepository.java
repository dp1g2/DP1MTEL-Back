package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Lot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface LotRepository extends JpaRepository<Lot, Long> {
    List<Lot> findByStore_Id(Long storeId);
    List<Lot> findByItem(Long itemId);
    Optional<Lot> findByStore_IdAndItem_Id(Long storeId, Long itemId);
    List<Lot> findByStore_IdAndItem_IdOrderByExpirationDateAsc(Long storeId, String itemId);
    List<Lot> findByStore_IdAndItem_IdOrderByCreatedAtAsc(Long storeId, String itemId);
}
