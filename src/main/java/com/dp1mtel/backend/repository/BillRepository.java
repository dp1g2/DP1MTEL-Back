package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillRepository extends JpaRepository<Bill,Long> {
}
