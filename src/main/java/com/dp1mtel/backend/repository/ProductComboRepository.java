package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Product;
import com.dp1mtel.backend.model.ProductCombo;
import com.dp1mtel.backend.model.ProductComboId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductComboRepository extends JpaRepository<ProductCombo,ProductComboId> {
    List<ProductCombo> findByPrimaryKey_Combo_Id (String comboId);
    List<ProductCombo> findByPrimaryKey_Combo_IdAndPrimaryKey_Product_ActiveTrue(String productId);
}
