package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.InventoryOperation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InventoryOperationRepository extends JpaRepository<InventoryOperation,Long> {
    List<InventoryOperation> findByStore_Id(Long storeId);
    List<InventoryOperation> findByStore_IdAndItem_IdAndInventoryOperationType_Name(Long storeId,String itemId, String inventoryOperaionTypeName);
}
