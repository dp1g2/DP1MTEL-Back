package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.QuantityDiscount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuantityDiscountRepository extends JpaRepository<QuantityDiscount, Long> {
}
