package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Customer;
import com.dp1mtel.backend.model.Store;
import com.dp1mtel.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface StoreRepository extends JpaRepository<Store, Long> {
    Optional<Store> findByDistrict(String district);
}
