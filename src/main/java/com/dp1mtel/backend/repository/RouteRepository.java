package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Route;
import com.dp1mtel.backend.model.Store;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RouteRepository extends JpaRepository<Route, Long> {
    List<Route> findAllByAssignedDriver_Store(Store store);
}
