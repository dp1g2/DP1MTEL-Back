package com.dp1mtel.backend.repository;

import com.dp1mtel.backend.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findByProductCategory_Id(String productCategoryId);
}
