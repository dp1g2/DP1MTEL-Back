package com.dp1mtel.backend.model;

import com.dp1mtel.backend.util.Point;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="\"order\"")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "seller_id")
    private User seller;

    @ManyToOne
    private Store store;

    @ManyToOne(cascade = {CascadeType.MERGE})
    private Customer customer;

    private String recipientName;
    private String recipientDedication;
    private String cancellationReason;

    @ManyToOne
    @JoinColumn(name = "order_status_id")
    private OrderStatus orderStatus;

    @OneToMany(mappedBy = "order",cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrderDetail> orderDetails = new ArrayList<>();

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt;

    @OneToOne
    private UserAddress deliveryAddress;

    private Date expectedDeliveryDate;

    @Column(unique = true)
    private String reservationCode;
    private Date reservationTime;

    /** Distribution related variables **/
    private double volume;
    private double userReadyTime;
    private double userServiceTime;
    private double userDueTime;

    public double getVolume() {
        volume = orderDetails.stream()
                .mapToDouble(OrderDetail::getVolume)
                .sum();
        return volume;
    }

    public void setVolume(double vol) {
        volume = vol;
    }

    public double getTotal() {
        Double total = 0.0;
        for (OrderDetail orderDetail : orderDetails) {
            total = total + orderDetail.getSubTotal();
        }
        return total;
    }

    @JsonIgnore
    public Point getCoordinates() {
        return deliveryAddress.getCoordinates();
    }

    public double getUserReadyTime() {
        return userReadyTime;
    }

    public void setUserReadyTime(double userReadyTime) {
        this.userReadyTime = userReadyTime;
    }

    public double getUserServiceTime() {
        return userServiceTime;
    }

    public void setUserServiceTime(double userServiceTime) {
        this.userServiceTime = userServiceTime;
    }

    public double getUserDueTime() {
        return userDueTime;
    }

    public void setUserDueTime(double userDueTime) {
        this.userDueTime = userDueTime;
    }

    /** Getters and setters **/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public void addOrderDetail(OrderDetail orderDetail) {
        Boolean isIn = false;
        if ( orderDetail.getCombo() != null) {
            for ( OrderDetail orderDetail1 : orderDetails)
                if (orderDetail1.getCombo()!=null && orderDetail1.getCombo().getId().equals(orderDetail.getCombo().getId()) )
                    isIn = true;

        } else if ( orderDetail.getProduct() != null){
            for ( OrderDetail orderDetail1 : orderDetails)
                if (orderDetail1.getProduct()!=null && orderDetail1.getProduct().getId().equals(orderDetail.getProduct().getId()) )
                    isIn = true;
        }
        if (!isIn) {
            orderDetail.setOrder(this);
            orderDetails.add(orderDetail);
        }
    }

    public void removeOrderDetail(OrderDetail orderDetail) {
        Boolean isIn = false;
        if ( orderDetail.getCombo() != null) {
            for ( OrderDetail orderDetail1 : orderDetails)
                if (orderDetail1.getCombo()!=null && orderDetail1.getCombo().getId().equals(orderDetail.getCombo().getId()))
                    isIn = true;

        } else if ( orderDetail.getProduct() != null){
            for ( OrderDetail orderDetail1 : orderDetails)
                if (orderDetail1.getProduct()!=null && orderDetail1.getProduct().getId().equals(orderDetail.getProduct().getId()) )
                    isIn = true;
        }
        if (isIn) {
            orderDetails.remove(orderDetail);
        }
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String orderDetailsLog() {
        return String.format(
                "Venta a Cliente %s por Vendedor %s en Tienda %s",
                getCustomer().getFullName(),
                getSeller().getFullName(),
                getStore().getAddress());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Order)) {
            return false;
        }
        return getId() == ((Order) obj).getId();
    }

    public OrderDetail findOrderDetail(Product product, Combo combo) {
        Integer index = -1;

        Integer i = 0;
        for (OrderDetail orderDetail : orderDetails) {
            if (orderDetail.getCombo() != null && combo != null && orderDetail.getCombo().getId().equals(combo.getId()))
                index = i;
            else if (orderDetail.getProduct() != null && product != null && orderDetail.getProduct().getId().equals(product.getId()))
                index = i;
            i++;
        }

        if (index == -1)
            return null;
        else
            return orderDetails.get(index);
    }

    @Override
    public int hashCode() {
        return getId().intValue();
    }

    public UserAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(UserAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getRecipientDedication() {
        return recipientDedication;
    }

    public void setRecipientDedication(String recipientDedication) {
        this.recipientDedication = recipientDedication;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public String getReservationCode() {
        return reservationCode;
    }

    public void setReservationCode(String reservationCode) {
        this.reservationCode = reservationCode;
    }

    public Date getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime(Date reservationTime) {
        this.reservationTime = reservationTime;
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }
}
