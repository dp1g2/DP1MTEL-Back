package com.dp1mtel.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "product_item")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.item",
            joinColumns = @JoinColumn(name = "item_id")),
        @AssociationOverride(name = "primaryKey.product",
            joinColumns = @JoinColumn(name = "product_id"))
})
public class ProductItem implements Serializable {

    @JsonIgnore
    private ProductItemId primaryKey = new ProductItemId();

    @NotNull
    private Integer quantity;

    @EmbeddedId
    public ProductItemId getPrimaryKey() { return primaryKey; }

    public void setPrimaryKey(ProductItemId primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Transient
    public Item getItem() { return getPrimaryKey().getItem(); }

    public void setItem(Item item) { getPrimaryKey().setItem(item); }

    @Transient
    @JsonIgnore
    public Product getProduct() { return getPrimaryKey().getProduct(); }

    public void setProduct(Product product) { getPrimaryKey().setProduct(product);}
}
