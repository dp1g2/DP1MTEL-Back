package com.dp1mtel.backend.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class ProductItemId implements Serializable {

    private Product product;
    private Item item;

    @ManyToOne
    @JsonIgnore
    public Product getProduct() { return product; }

    public void setProduct(Product product) {
        this.product = product;
    }

    @ManyToOne
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
