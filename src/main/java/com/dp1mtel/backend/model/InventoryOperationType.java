package com.dp1mtel.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.annotation.Generated;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "inventory_operation_type")
public class InventoryOperationType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    @NotNull
    private Integer multiplier;

    @NotNull
    @JsonIgnore
    private Boolean createsNewLot;

    public InventoryOperationType(){}

    public InventoryOperationType(String name, String description, Integer multiplier, Boolean newLot) {
        setDescription(description);
        setName(name);
        setMultiplier(multiplier);
        setCreatesNewLot(newLot);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Integer multiplier) {
        this.multiplier = multiplier;
    }

    public Boolean getCreatesNewLot() {
        return createsNewLot;
    }

    public void setCreatesNewLot(Boolean createsNewLot) {
        this.createsNewLot = createsNewLot;
    }
}
