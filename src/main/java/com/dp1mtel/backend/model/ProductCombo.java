package com.dp1mtel.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="product_combo")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.product",
            joinColumns = @JoinColumn(name = "product_id")),
        @AssociationOverride(name = "primaryKey.combo",
            joinColumns = @JoinColumn(name = "combo_id"))
})
public class ProductCombo implements Serializable {

    @JsonIgnore
    private ProductComboId primaryKey = new ProductComboId();

    @Column
    private Integer quantity;

    @EmbeddedId
    public ProductComboId getPrimaryKey() { return primaryKey; }

    public void setPrimaryKey(ProductComboId primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Transient
    public Product getProduct() { return getPrimaryKey().getProduct(); }

    public void setProduct(Product product) { getPrimaryKey().setProduct(product); }

    @Transient
    @JsonIgnore
    public Combo getCombo() { return getPrimaryKey().getCombo(); }

    public void setCombo(Combo combo) { getPrimaryKey().setCombo(combo); }

}
