package com.dp1mtel.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name = "combo")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt","updatedAt"}, allowGetters = true)
public class Combo implements Serializable, IProduct {

    @Id
    private String id;

    @NotBlank
    private String name;

    @NotNull
    private Double price;

    @NotBlank
    private String description;

    @NotNull
    private Boolean active;

    @OneToMany(mappedBy = "primaryKey.combo", cascade = {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REMOVE})
    private List<ProductCombo> productCombos = new ArrayList<>();

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt;

    private Integer stock;

    @Override
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProductCombo> getProductCombos() {
        return productCombos;
    }

    public void addProduct(Product product,Integer quantity) {
        ProductCombo productCombo = new ProductCombo();
        productCombo.setProduct(product);
        productCombo.setCombo(this);
        productCombo.setQuantity(quantity);
        productCombos.add(productCombo);
    }

    public void removeProduct(Product product) {

        List<ProductCombo> productComboList = new ArrayList<>(productCombos);

        for (ProductCombo productCombo : productComboList)
            if (productCombo.getProduct().getId().equals(product.getId()))
                productCombos.remove(productCombo);

        for (ProductCombo productCombo : productCombos)
            if (productCombo.getProduct().getId().equals(product.getId()) )
                productCombos.remove(productCombo);
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Integer getStock() {
        stock = productCombos.stream()
                .map((productCombo) -> new Integer[] {
                        productCombo.getQuantity(),
                        productCombo.getProduct().getStock()
                })
                .map((needXStock) -> needXStock[1] / needXStock[0])
                .min(Comparator.comparingInt(a -> a))
                .get();
        return stock;
    }

    @Override
    public double getVolume() {
        return productCombos.stream()
                .mapToDouble(pCombo -> pCombo.getProduct().getVolume()*pCombo.getQuantity())
                .sum();
    }

    @Override
    public List<ProductItem> getItems() {
        return productCombos.stream()
                .map(ProductCombo::getProduct)
                .map(Product::getItems)
                .flatMap(List::stream)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
