package com.dp1mtel.backend.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Entity
@Table(name = "bill")
public class Bill implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Long codeBoleta;

    private Long codeFactura;

    private Long codeNotaCredito;

    public Long getCodeBoleta() {
        return codeBoleta;
    }

    public void setCodeBoleta(Long codeBoleta) {
        this.codeBoleta = codeBoleta;
    }

    public Long getCodeFactura() {
        return codeFactura;
    }

    public void setCodeFactura(Long codeFactura) {
        this.codeFactura = codeFactura;
    }

    public Long getCodeNotaCredito() {
        return codeNotaCredito;
    }

    public void setCodeNotaCredito(Long codeNotaCredito) {
        this.codeNotaCredito = codeNotaCredito;
    }
}
