package com.dp1mtel.backend.model;

import java.io.Serializable;

public class PosItem implements Serializable {

    private Item item;

    private Integer stock;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }


    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
}
