package com.dp1mtel.backend.model;

import java.io.Serializable;

public class PosProduct implements Serializable {

    private Product product;

    private Integer stock;

    private Double discount;


    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
