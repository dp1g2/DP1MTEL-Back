package com.dp1mtel.backend.model;

import java.io.Serializable;

public class PosCombo implements Serializable {

    private Combo combo;
    private Integer stock;
    private Double discount;

    public Combo getCombo() {
        return combo;
    }

    public void setCombo(Combo combo) {
        this.combo = combo;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
