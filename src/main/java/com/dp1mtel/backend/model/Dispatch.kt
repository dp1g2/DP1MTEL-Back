package com.dp1mtel.backend.model

class Dispatch() {
    var routes: List<Route> = listOf()

    constructor(routes: List<Route>): this() {
        this.routes = routes
    }
}