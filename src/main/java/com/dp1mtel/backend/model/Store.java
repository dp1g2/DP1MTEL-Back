package com.dp1mtel.backend.model;

import com.dp1mtel.backend.util.Point;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="store")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt","numEmployees"}, allowGetters = true)
public class Store implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotBlank
    private String address;

    @NotEmpty
    private String district;

    @NotNull
    private Boolean active;

    private Integer numEmployees;

    @NotNull
    private Integer numVehicles;

    @NotNull
    private Integer vehiclesCapacity;

    @OneToMany(mappedBy = "store")
    private List<User> users = new ArrayList<>();

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt;

    private double latitude;
    private double longitude;
    private double closingTime = Double.POSITIVE_INFINITY;


    public Point getCoordinates() {
        return new Point(0, latitude, longitude);

    }

    public void setCoordinates(Point coordinates) {
        this.latitude = coordinates.getX();
        this.longitude = coordinates.getY();
    }

    // TODO: Set either by store, or by default
    public double getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(double closingTime) {
        this.closingTime = closingTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonIgnore
    public List<User> getUsers() {
        return users;
    }

    @JsonProperty
    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addUser(User user) {
        if (!users.contains(user))
            users.add(user);
    }

    public void removeUser(User user) {
        if (users.contains(user))
            users.remove(user);
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getNumVehicles() {
        return numVehicles;
    }

    public void setNumVehicles(Integer numVehicles) {
        this.numVehicles = numVehicles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVehiclesCapacity() {
        return vehiclesCapacity;
    }

    public void setVehiclesCapacity(Integer vehiclesCapacity) {
        this.vehiclesCapacity = vehiclesCapacity;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Integer getNumEmployees() {
        return numEmployees;
    }

    public void setNumEmployees(Integer numEmployees) {
        this.numEmployees = numEmployees;
    }
}
