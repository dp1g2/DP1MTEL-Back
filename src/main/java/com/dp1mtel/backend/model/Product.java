package com.dp1mtel.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

@Entity
@Table(name="product")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class Product implements Serializable, IProduct {

    @Id
    private String id;

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @NotNull
    private  Double price;

    @NotNull
    private Boolean active;

    @OneToMany(mappedBy = "primaryKey.product", cascade = {CascadeType.MERGE}, orphanRemoval = true)
    private List<ProductItem> productItems = new ArrayList<>();

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    private Date updatedAt;

    @OneToMany(mappedBy = "primaryKey.product", cascade = CascadeType.DETACH)
    @JsonIgnore
    private List<ProductCombo> productCombos = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "product_category_id")
    private ProductCategory productCategory;

    private Integer stock;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<ProductCombo> getProductCombos() {
        return productCombos;
    }

    public void addItem(Item item, Integer quantity) {
        ProductItem productItem = new ProductItem();
        productItem.setItem(item);
        productItem.setProduct(this);
        productItem.setQuantity(quantity);
        getProductItems().add(productItem);
    }

    public void removeItem(Item item) {

        List<ProductItem> productItemList = new ArrayList<>(productItems);

        for (ProductItem productItem : productItemList)
            if (productItem.getItem().getId().equals(item.getId()))
                productItems.remove(productItem);

    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ProductItem> getProductItems() {
        return productItems;
    }

    public void setProductItems(List<ProductItem> productItems) {
        this.productItems = productItems;
    }

    @Override
    public Integer getStock() {
        stock = productItems.stream()
                .map((pItem) -> new Integer[] { pItem.getQuantity(), pItem.getItem().getStock() })
                .map((needXStock) -> needXStock[1] / needXStock[0])
                .min(Comparator.comparingInt(a -> a))
                .get();
        return stock;
    }

    @Override
    public double getVolume() {
        return productItems.stream()
                .mapToDouble(productItem -> productItem.getItem().getVolume()*productItem.getQuantity())
                .sum();
    }

    @Override
    public List<ProductItem> getItems() {
        return productItems;
    }
}
