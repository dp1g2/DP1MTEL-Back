package com.dp1mtel.backend.model;

import java.util.List;

public interface IProduct {
    String getId();
    String getName();
    String getDescription();
    Double getPrice();
    Integer getStock();
    double getVolume();
    List<ProductItem> getItems();
}
