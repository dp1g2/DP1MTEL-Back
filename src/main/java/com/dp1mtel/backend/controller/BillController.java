package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.model.Bill;
import com.dp1mtel.backend.repository.BillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class BillController {

    @Autowired
    BillRepository billRepository;

    @PostMapping("/bill")
    public ResponseEntity<?> generateBill(@Param(value = "type") String type){
        //BOLETA
        //FACTURA
        //NOTA_CREDITO
        Bill bill = billRepository.findAll().get(0);
        switch (type){
            case "BOLETA":
                bill.setCodeBoleta(bill.getCodeBoleta()+1);
                break;
            case "FACTURA":
                bill.setCodeFactura(bill.getCodeFactura()+1);
                break;
            case "NOTA_CREDITO":
                bill.setCodeNotaCredito(bill.getCodeNotaCredito()+1);
                break;
        }
        billRepository.save(bill);

        return ResponseEntity.ok(bill);
    }
}
