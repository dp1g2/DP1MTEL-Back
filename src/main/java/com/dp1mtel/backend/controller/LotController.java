package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.Lot;
import com.dp1mtel.backend.repository.LotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class LotController {
    @Autowired
    LotRepository lotRepository;

    @GetMapping("/lots")
    public List<Lot> getAllLots() { return lotRepository.findAll(); }

    @PostMapping("/lots")
    public  Lot createLot(@Valid @RequestBody Lot lot) { return lotRepository.save(lot); }

    @GetMapping("/lots/{id}")
    public Lot getLotById(@PathVariable(value = "id") Long lotId) {
        return lotRepository.findById(lotId)
                .orElseThrow(() -> new ResourceNotFoundException("Lot","id",lotId));
    }

    @PatchMapping("/lots/{id}")
    public Lot partialUpdate(@PathVariable(value = "id") Long lotId, @Valid @RequestBody Lot lotDetails) {
        Lot lot = lotRepository.findById(lotId)
                .orElseThrow(() -> new ResourceNotFoundException("Lot","id",lotId));

        lot.setQuantity(lotDetails.getQuantity());

        Lot updatedLot = lotRepository.save(lot);
        return updatedLot;
    }
}
