package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.Permission;
import com.dp1mtel.backend.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/permissions")
public class PermissionController {
    @Autowired
    PermissionRepository permissionRepository;

    @GetMapping("")
    public List<Permission> getAllPermissions() {
        return permissionRepository.findAll();
    }

    @PostMapping("")
    public Permission createPermission(@Valid @RequestBody Permission permission) {
        return permissionRepository.save(permission);
    }

    @GetMapping("/{id}")
    public Permission getPermissionById(@PathVariable(value = "id") Long permissionId) {
        return permissionRepository.findById(permissionId)
                .orElseThrow(() -> new ResourceNotFoundException("Permission", "id", permissionId));
    }

    @PutMapping("/{id}")
    public Permission updatePermission(@PathVariable(value = "id") Long permissionId,
                                       @Valid @RequestBody Permission newPermission) {
        Permission permission = permissionRepository.findById(permissionId)
                .orElseThrow(() -> new ResourceNotFoundException("Permission", "id", permissionId));

        permission.update(newPermission);

        return permissionRepository.save(permission);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletePermission(@PathVariable(value = "id") Long permissionId) {
        Permission permission = permissionRepository.findById(permissionId)
                .orElseThrow(() -> new ResourceNotFoundException("Permission", "id", permissionId));
        permissionRepository.delete(permission);
        return ResponseEntity.ok().build();
    }
}
