package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceConflictException;
import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.*;
import com.dp1mtel.backend.repository.*;
import com.dp1mtel.backend.responses.MassiveStorageResponse;
import okhttp3.MultipartBody;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.mail.Multipart;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    ProductRepository productRepository;
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    ProductCategoryRepository productCategoryRepository;
    @Autowired
    ProductItemRepository productItemRepository;
    @Autowired
    ProductComboRepository productComboRepository;

    private static String UPLOAD_FOLDER = "/home/inf226/storedImages/";

    @GetMapping("/products")
    public List<Product> getAllProducts() {
        return productRepository.findAll().stream().filter(product -> {
            return product.getActive();
        }).collect(Collectors.toList());
    }

    @PostMapping("/products/uploadImage")
    public ResponseEntity<String> uploadProductImage(@RequestParam("image") MultipartFile file, @RequestParam("id") String idDescrip) {

        try {
            byte[] bytes = file.getBytes();
            //Find extension file.getOriginalFilename()
            String fileName = file.getOriginalFilename();
            String fileExtension =  "." + fileName.substring(fileName.lastIndexOf('.') + 1) ;
//            System.out.println("The file extension is " + fileExtension);
//            System.out.println("Full file is "+ idDescrip + fileExtension);
            Path path = Paths.get(UPLOAD_FOLDER + idDescrip + fileExtension.toLowerCase());
            Files.write(path, bytes);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/products")
    public Product createProduct(@Valid @RequestBody Product product) {
        Optional<Product> productOptional = productRepository.findById(product.getId());

        if (productOptional.isPresent())
            throw new ResourceConflictException("Product", product.getId());

        ProductCategory productCategory = productCategoryRepository.findById(product.getProductCategory().getId())
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategory", "id", product.getProductCategory().getId()));

        Product newProduct = new Product();
        newProduct.setName(product.getName());
        newProduct.setActive(product.getActive());
        newProduct.setPrice(product.getPrice());
        newProduct.setDescription(product.getDescription());
        newProduct.setProductCategory(productCategory);
        newProduct.setId(product.getId());

        for (ProductItem productItem : product.getProductItems()) {
            Item item = itemRepository.findById(productItem.getItem().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Item", "id", productItem.getItem().getId()));
            newProduct.addItem(item, productItem.getQuantity());
        }

        return productRepository.save(newProduct);
    }

    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable(value = "id") String productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("product", "id", productId));
    }

    @PutMapping("/products/{id}")
    public Product updateProduct(@PathVariable(value = "id") String productId,
                                 @Valid @RequestBody Product productDetails) {

        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", productId));

        ProductCategory productCategory = productCategoryRepository.findById(productDetails.getProductCategory().getId())
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategory", "id", productDetails.getProductCategory().getId()));

        product.setDescription(productDetails.getDescription());
        product.setName(productDetails.getName());
        product.setPrice(productDetails.getPrice());
        product.setActive(productDetails.getActive());
        product.setProductCategory(productCategory);

        List<ProductItem> productItemList = new ArrayList<>(product.getProductItems());

        for (ProductItem productItem : productItemList)
            product.removeItem(productItem.getItem());


        productItemRepository.deleteAll(productItemRepository.findByPrimaryKey_Product_Id(product.getId()));

        for (ProductItem productItem : productDetails.getProductItems()) {

            Item newItem = itemRepository.findById(productItem.getItem().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Item", "id", productItem.getItem().getId()));

            product.addItem(newItem, productItem.getQuantity());

        }


        Product updatedProduct = productRepository.save(product);
        return updatedProduct;

    }

    @PatchMapping("/products/{id}")
    public ResponseEntity<?> disableProduct(@PathVariable(value = "id") String productId, Product productToDisable) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", productId));
        List<ProductCombo> productComboList = productComboRepository.findByPrimaryKey_Combo_IdAndPrimaryKey_Product_ActiveTrue(productId);
        if (productComboList.size() > 0)
            return ResponseEntity.badRequest().build();
        product.setActive(false);
        product = productRepository.save(product);
        return ResponseEntity.ok(product);
    }

    @PostMapping("/products/bulk")
    public ResponseEntity<MassiveStorageResponse> bulkSave(@RequestBody List<Product> products) {
        int correctlyInserted = 0;
        int badlyInserted = 0;
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            Product prod = products.get(i);
            try {
                Optional<Product> productOptional = productRepository.findById(prod.getId());

                if (productOptional.isPresent())
                    throw new ResourceConflictException("Product", prod.getId());

                ProductCategory productCategory = productCategoryRepository.findById(prod.getProductCategory().getId())
                        .orElseThrow(() -> new ResourceNotFoundException("ProductCategory", "id", prod.getProductCategory().getId()));

                Product newProduct = new Product();
                newProduct.setName(prod.getName());
                newProduct.setActive(prod.getActive());
                newProduct.setPrice(prod.getPrice());
                newProduct.setDescription(prod.getDescription());
                newProduct.setProductCategory(productCategory);
                newProduct.setId(prod.getId());

                for (ProductItem productItem : prod.getProductItems()) {
                    Item item = itemRepository.findById(productItem.getItem().getId())
                            .orElseThrow(() -> new ResourceNotFoundException("Item", "id", productItem.getItem().getId()));
                    newProduct.addItem(item, productItem.getQuantity());
                }
                productRepository.save(newProduct);
                correctlyInserted++;

            } catch (Exception ex) {
                badlyInserted++;
                positions.add(i);
            }
        }
        return ResponseEntity.ok(new MassiveStorageResponse(String.valueOf(correctlyInserted),
                String.valueOf(badlyInserted), positions));
    }


    @DeleteMapping("/products/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable(value = "id") String productId) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new ResourceNotFoundException("product", "id", productId));

        try {
            productRepository.delete(product);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return ResponseEntity.ok().build();
    }
}
