package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceConflictException;
import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.Product;
import com.dp1mtel.backend.model.ProductCategory;
import com.dp1mtel.backend.repository.ProductCategoryRepository;
import com.dp1mtel.backend.repository.ProductRepository;
import com.dp1mtel.backend.responses.MassiveStorageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ProductCategoryController {

    @Autowired
    ProductCategoryRepository productCategoryRepository;
    @Autowired
    ProductRepository productRepository;

    @GetMapping("/product-categories")
    public List<ProductCategory> getAllProductCategories(@Param(value = "name") String name) {
        if (name == null)
            return productCategoryRepository.findAll().stream().filter( productCategory -> {return productCategory.getActive();}).collect(Collectors.toList());
        else
            return productCategoryRepository.findByNameEquals(name).stream().filter( productCategory -> {return productCategory.getActive();}).collect(Collectors.toList());
    }

    @GetMapping("/product-categories/{id}")
    public ProductCategory getProductCategoryById(@PathVariable(value = "id") String productCategoryId) {
        return productCategoryRepository.findById(productCategoryId)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategory","id",productCategoryId));

    }

    @PatchMapping("/product-categories/{id}")
    public ResponseEntity<?> disableProductCategory(@PathVariable(value = "id") String productCategoryId,
                                                    @Valid @RequestBody ProductCategory productCategoryToDisable) {
        ProductCategory productCategory = productCategoryRepository.findById(productCategoryId)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategory","id",productCategoryId));
        List<Product> productList = productRepository.findByProductCategory_Id(productCategory.getId());
        if (productList.size() > 0)
            return ResponseEntity.badRequest().build();
        productCategoryToDisable.setActive(false);
        productCategoryRepository.save(productCategoryToDisable);
        return ResponseEntity.ok(productCategoryToDisable);
    }


    @PostMapping("/product-categories")
    public ProductCategory createProductCategory(@Valid @RequestBody ProductCategory productCategory) {
        Optional<ProductCategory> productCategoryOptional = productCategoryRepository.findById(productCategory.getId());

        if (productCategoryOptional.isPresent())
            throw new ResourceConflictException("ProductCategory",productCategory.getId());

        return productCategoryRepository.save(productCategory);
    }

    @PutMapping("/product-categories/{id}")
    public ProductCategory updateProductCategory(@PathVariable(value = "id") String productCategoryId,
                                                 @Valid @RequestBody ProductCategory productCategoryDetails) {
        ProductCategory productCategory = productCategoryRepository.findById(productCategoryId)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategory","id",productCategoryId));
        productCategory.setName(productCategoryDetails.getName());
        productCategory.setDescription(productCategoryDetails.getDescription());
        productCategory.setId(productCategoryDetails.getId());

        ProductCategory updatedProductCategory = productCategoryRepository.save(productCategory);
        return updatedProductCategory;
    }

    @DeleteMapping("/product-categories/{id}")
    public ResponseEntity<?> deleteProductCategory(@PathVariable(value = "id") String  productCategoryId) {
        ProductCategory productCategory = productCategoryRepository.findById(productCategoryId)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategory","id",productCategoryId));

        try {
            productCategoryRepository.delete(productCategory);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping("/product-categories/bulk")
    public ResponseEntity<MassiveStorageResponse> bulkSave(@RequestBody List<ProductCategory> categories) {
        int correctlyInserted = 0;
        int badlyInserted = 0;
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < categories.size(); i++) {
            ProductCategory cat = categories.get(i);
            try {
                Optional<ProductCategory> productCategoryOptional = productCategoryRepository.findById(cat.getId());
                if (!productCategoryOptional.isPresent()) {
                    productCategoryRepository.save(cat);
                    correctlyInserted++;
                } else {
                    badlyInserted++;
                    positions.add(i);
                }
            } catch (Exception ex) {
                badlyInserted++;
                positions.add(i);
            }
        }
        return ResponseEntity.ok(new MassiveStorageResponse(String.valueOf(correctlyInserted),
                String.valueOf(badlyInserted),positions));
    }
}
