package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.model.Customer;
import com.dp1mtel.backend.model.User;
import com.dp1mtel.backend.repository.CustomerRepository;
import com.dp1mtel.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Optional;

@Controller
public class PasswordController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    CustomerRepository customerRepository;
//
//    @Autowired
//    private EmailService emailService;

//    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

//    @RequestMapping(value = "/forgot", method = RequestMethod.GET)
//    public ModelAndView displayForgotPasswordPage() {
//        return new ModelAndView("forgotPassword");
//    }
//
//    // Process form submission from forgotPassword page
//    @RequestMapping(value = "/forgot", method = RequestMethod.POST)
//    public ModelAndView processForgotPasswordForm(ModelAndView modelAndView, @RequestParam("email") String userEmail, HttpServletRequest request) {
//
//        // Lookup user in database by e-mail
//        Optional<User> optional = userRepository.findByEmail(userEmail);
//
//        if (!optional.isPresent()) {
//            modelAndView.addObject("errorMessage", "We didn't find an account for that e-mail address.");
//        } else {
//
//            // Generate random 36-character string token for reset password
//            User user = optional.get();
//            user.setResetToken(UUID.randomUUID().toString());
//
//            // Save token to database
//            userRepository.save(user);
//
//            String appUrl = request.getScheme() + "://" + request.getServerName();
//
//            // Email message
//            SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
//            passwordResetEmail.setFrom("margaritatel.team@gmail.com");
//            passwordResetEmail.setTo(user.getEmail());
//            passwordResetEmail.setSubject("Password Reset Request");
//            passwordResetEmail.setText("To reset your password, click the link below:\n" + appUrl
//                    + "/reset?token=" + user.getResetToken());
//
//            emailService.sendEmail(passwordResetEmail);
//
//            // Add success message to view
//            modelAndView.addObject("successMessage", "A password reset link has been sent to " + userEmail);
//        }
//
//        modelAndView.setViewName("forgotPassword");
//        return modelAndView;
//
//    }

    // Display form to reset password
    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public ModelAndView displayResetPasswordPage(ModelAndView modelAndView, @RequestParam("token") String token) {

        Optional<User> user = userRepository.findByResetToken(token);

        if (user.isPresent()) { // Token found in DB
            modelAndView.addObject("resetToken", token);
            modelAndView.addObject("error", false);
        } else { // Token not found in DB
            modelAndView.addObject("error", true);
        }

        modelAndView.setViewName("resetPassword");
        return modelAndView;
    }

    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public ModelAndView displaySuccessful(ModelAndView modelAndView) {
        modelAndView.setViewName("success");
        return modelAndView;
    }
    // Process reset password form
    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public ModelAndView setNewPassword(ModelAndView modelAndView, @RequestParam Map<String, String> requestParams, RedirectAttributes redir) {

        // Find the user associated with the reset token
        Optional<User> user = userRepository.findByResetToken(requestParams.get("token"));

        // This should always be non-null but we check just in case
        if (user.isPresent()) {

            User resetUser = user.get();

            // Set new password
            resetUser.setPassword(requestParams.get("password"));
            try {
                resetUser.generatePassword();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            // Set the reset token to null so it cannot be used again
            resetUser.setResetToken(null);

            // Save user
            userRepository.save(resetUser);

            modelAndView.setViewName("redirect:success");
            return modelAndView;

        } else {
            modelAndView.addObject("errorMessage", "Oops!  This is an invalid password reset link.");
            modelAndView.setViewName("resetPassword");
        }

        return modelAndView;
    }

    //PARA CUSTOMERS
    // ========================================================================================================

    @RequestMapping(value = "/resetCustomerPassword", method = RequestMethod.GET)
    public ModelAndView displayResetPasswordPageCustomer(ModelAndView modelAndView, @RequestParam("token") String token) {

        Optional<Customer> customer = customerRepository.findByResetToken(token);

        if (customer.isPresent()) { // Token found in DB
            modelAndView.addObject("resetToken", token);
            modelAndView.addObject("error", false);
        } else { // Token not found in DB
            modelAndView.addObject("error", true);
        }

        modelAndView.setViewName("resetPasswordCustomer");
        return modelAndView;
    }

    // Process reset password form
    @RequestMapping(value = "/resetCustomerPassword", method = RequestMethod.POST)
    public ModelAndView setNewPasswordCustomer(ModelAndView modelAndView, @RequestParam Map<String, String> requestParams, RedirectAttributes redir) {

        // Find the user associated with the reset token
        Optional<Customer> customer = customerRepository.findByResetToken(requestParams.get("token"));

        // This should always be non-null but we check just in case
        if (customer.isPresent()) {

            Customer resetCustomer = customer.get();

            // Set new password
            resetCustomer.setPassword(requestParams.get("password"));

            // Set the reset token to null so it cannot be used again
            resetCustomer.setResetToken(null);

            // Save user
            customerRepository.save(resetCustomer);

            modelAndView.setViewName("redirect:success");
            return modelAndView;

        } else {
            modelAndView.addObject("errorMessage", "Oops!  This is an invalid password reset link.");
            modelAndView.setViewName("resetPasswordCustomer");
        }

        return modelAndView;
    }


}
