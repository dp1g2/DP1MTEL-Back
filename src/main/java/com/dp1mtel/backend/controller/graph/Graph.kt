package com.dp1mtel.backend.controller.graph

import com.dp1mtel.backend.util.toGHPoint
import com.dp1mtel.backend.util.toPoint2D
import com.graphhopper.GHRequest
import com.graphhopper.GraphHopperAPI
import com.graphhopper.PathWrapper
import com.graphhopper.api.GraphHopperGeocoding
import com.graphhopper.api.GraphHopperWeb
import com.graphhopper.api.model.GHGeocodingEntry
import com.graphhopper.api.model.GHGeocodingRequest
import com.graphhopper.reader.osm.GraphHopperOSM
import com.graphhopper.routing.util.EncodingManager
import com.graphhopper.routing.util.FlagEncoderFactory
import com.graphhopper.util.shapes.GHPoint
import org.springframework.stereotype.Component

import java.awt.geom.Point2D
import java.util.*

@Component
class Graph {
    enum class MODE {
        LOCAL,
        API
    }

    companion object {
        @JvmField val MAX_CUSTOMERS_PER_ROUTE = 30
        @JvmField val API_KEY = "2af8c5f2-6fd2-4a89-9df5-224268cb648b"
    }

    private var routingService: GraphHopperAPI = initApi()
    private var geocodingService = GraphHopperGeocoding().apply {
        setKey(API_KEY)
    }
    private var mode: MODE = MODE.API

    fun init(mode: MODE = MODE.API) {
        this.mode = mode
        when (mode) {
            MODE.LOCAL -> initHopper()
            MODE.API -> initApi()
        }
    }

    private fun initHopper(): GraphHopperAPI {
        val fileLocation = "maps"
        val mapsLocation = "maps"
        routingService = GraphHopperOSM().forServer().apply {
            dataReaderFile = fileLocation
            graphHopperLocation = mapsLocation
            encodingManager = EncodingManager(FlagEncoderFactory.CAR)
            importOrLoad()
        }
        return routingService
    }

    private fun initApi(): GraphHopperAPI {
        routingService = GraphHopperWeb().apply {
            setKey(API_KEY)
        }
        return routingService
    }

    fun route(vararg points: GHPoint): Optional<PathWrapper> {
        return route(*points.map { it.toPoint2D() }.toTypedArray())
    }

    fun route(vararg points: Point2D): Optional<PathWrapper> {
        val req = GHRequest().apply {
            points.forEach { point -> addPoint(point.toGHPoint()) }
            weighting = "fastest"
            vehicle = "car"
            locale = Locale.getDefault()
        }

        val res = routingService.route(req)
        if (res.hasErrors()) {
            // TODO: handle errors
            println(res.errors.map { it.message })
            return Optional.empty()
        }
        return Optional.of(res.best)
    }

    fun geocode(address: String, locale: String = "en", limit: Int = 5): List<GHGeocodingEntry> {
        val req = GHGeocodingRequest(address, locale, limit)
        val res = geocodingService.geocode(req)
        return res.hits ?: listOf()
    }
}
