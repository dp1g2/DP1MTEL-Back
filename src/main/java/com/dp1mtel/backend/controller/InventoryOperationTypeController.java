package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.InventoryOperationType;
import com.dp1mtel.backend.repository.InventoryOperationTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/inventory-operation-types")
public class InventoryOperationTypeController {

    @Autowired
    InventoryOperationTypeRepository inventoryOperationTypeRepository;

    @GetMapping("")
    public List<InventoryOperationType> getAllInventoryOperationTypes() { return inventoryOperationTypeRepository.findAll(); }

    @GetMapping("/{id}")
    public InventoryOperationType getInventoryOperationTypeById(@PathVariable(value = "id") Long inventoryOperationTypeId) {
        return inventoryOperationTypeRepository.findById(inventoryOperationTypeId)
                .orElseThrow(() -> new ResourceNotFoundException("InventoryOperationType","id",inventoryOperationTypeId));
    }
}
