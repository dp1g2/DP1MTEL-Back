package com.dp1mtel.backend.controller

import com.dp1mtel.backend.exception.ResourceConflictException
import com.dp1mtel.backend.exception.ResourceNotFoundException
import com.dp1mtel.backend.model.Customer
import com.dp1mtel.backend.model.UserAddress
import com.dp1mtel.backend.repository.AddressRepository
import com.dp1mtel.backend.repository.CustomerRepository
import com.dp1mtel.backend.responses.MassiveStorageResponse
import com.dp1mtel.backend.service.EmailService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.mail.SimpleMailMessage
import org.springframework.web.bind.annotation.*
import java.util.*
import java.util.concurrent.CompletableFuture

import javax.validation.Valid

@RestController
@RequestMapping("/api/customers")
class CustomerController {
    @Autowired
    private lateinit var customerRepository: CustomerRepository
    @Autowired
    private lateinit var addressRepository: AddressRepository
    @Autowired
    private lateinit var emailService: EmailService

    @GetMapping("")
    fun index(): List<Customer> {
        return customerRepository.findAll()
    }

    @PostMapping("")
    fun store(@Valid @RequestBody customer: Customer): Customer {
        customerRepository.findById(customer.id).ifPresent { throw ResourceConflictException("Customer", customer.id) }
        customerRepository.findByDocument(customer.document).ifPresent { throw ResourceConflictException("Customer", customer.document) }
        customerRepository.findByPhone(customer.phone).ifPresent { throw ResourceConflictException("Customer", customer.phone) }
        return customerRepository.save(customer)
    }

    @PostMapping("/bulk")
    fun bulkSave(@Valid @RequestBody customers: List<Customer>): ResponseEntity<MassiveStorageResponse> {
//        var tokenTest = ""
        var correctlyInserted = 0
        var badlyInserted = 0
        val positions = ArrayList<Int>()
        var i =0
        for (customer in customers){
            try{
                customerRepository.findByDocument(customer.document).ifPresent { throw ResourceConflictException("Customer", customer.document) }
                customerRepository.findByPhone(customer.phone).ifPresent { throw ResourceConflictException("Customer", customer.phone) }


//                customer.resetToken = UUID.randomUUID().toString()
//                tokenTest = customer.resetToken
//
//                val passwordResetEmail = SimpleMailMessage()
//                passwordResetEmail.from = "margaritatel.team@gmail.com"
//                passwordResetEmail.setTo(customer.email)
//                passwordResetEmail.subject = "Restablecer contraseña acceso"
//                passwordResetEmail.text = "Por favor, ingrese al siguiente link para reestablecer su contrasena. " +
//                        "http://200.16.7.147/resetCustomerPassword?token=" + tokenTest

                customerRepository.save(customer)

//                CompletableFuture.supplyAsync<Any> {
//                    emailService.sendEmail(passwordResetEmail)
//                    null
//                }

                correctlyInserted++
            }catch(ex:Exception){
                badlyInserted++
                positions.add(i++)
            }
        }
        return ResponseEntity.ok(MassiveStorageResponse(correctlyInserted.toString(),
                badlyInserted.toString(), positions))
    }



    @GetMapping("/{id}")
    fun show(@PathVariable("id") id: Long): Customer {
        val customer = customerRepository.findById(id).orElseThrow({ ResourceNotFoundException("Customer", "id", id) })
        return customer
    }

    @PostMapping("/{id}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody newCustomer: Customer): Customer {
        val customer = customerRepository.findById(id).orElseThrow { ResourceNotFoundException("Customer", "id", id) }
        customer.apply {
            firstName = newCustomer.firstName
            lastName = newCustomer.lastName
            document = newCustomer.document
            email = newCustomer.email
            phone = newCustomer.phone
            val newOrders = newCustomer.orders.toSet()
            val orders = orders.toSet()
            customer.orders.addAll(newOrders.minus(orders))
        }
        return customerRepository.save(customer)
    }

    @GetMapping("/{id}/addresses")
    fun addresses(@PathVariable("id") id: Long): List<UserAddress> {
        val customer = customerRepository.findById(id).orElseThrow { ResourceNotFoundException("Customer", "id", id) }
        return customer.addresses
    }

    @PostMapping("/{id}/addresses")
    fun saveAddress(@PathVariable("id") id: Long, @Valid @RequestBody address: UserAddress): UserAddress {
        val customer = customerRepository.findById(id).orElseThrow { ResourceNotFoundException("Customer", "id", id) }
        val newAddress = UserAddress().apply {
            name = address.name
            this.address = address.address
            this.customer = customer
            this.coordinates = address.coordinates
        }
        // When saved through address repository, customer.addresses does not update. Why ?
        customer.addresses.add(newAddress)
        customerRepository.save(customer)
        return customer.addresses.last()
    }
}
