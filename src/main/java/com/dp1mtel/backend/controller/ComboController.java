package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceConflictException;
import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.Combo;
import com.dp1mtel.backend.model.Product;
import com.dp1mtel.backend.model.ProductCombo;
import com.dp1mtel.backend.repository.ComboRepository;
import com.dp1mtel.backend.repository.ProductComboRepository;
import com.dp1mtel.backend.repository.ProductRepository;
import com.dp1mtel.backend.responses.MassiveStorageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ComboController {
    @Autowired
    ComboRepository comboRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductComboRepository productComboRepository;

    private static String UPLOAD_FOLDER = "/home/inf226/storedImages/";

    @GetMapping("/combos")
    public List<Combo> getAllCombos(){ return comboRepository.findAll().stream().filter( combo -> {return combo.getActive();}).collect(Collectors.toList()); }

    @PostMapping("/combos/uploadImage")
    public ResponseEntity<String> uploadProductImage(@RequestParam("image") MultipartFile file, @RequestParam("id") String idDescrip) {

        try {
            byte[] bytes = file.getBytes();
            String fileName = file.getOriginalFilename();
            String fileExtension =  "." + fileName.substring(fileName.lastIndexOf('.') + 1) ;
            Path path = Paths.get(UPLOAD_FOLDER + idDescrip + fileExtension.toLowerCase());
            Files.write(path, bytes);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping("/combos")
    public Combo createCombo(@Valid @RequestBody Combo combo) {
        Optional<Combo> comboOptional = comboRepository.findById(combo.getId());

        if (comboOptional.isPresent())
            throw  new ResourceConflictException("Combo",combo.getId());

        Combo newCombo = new Combo();
        newCombo.setName(combo.getName());
        newCombo.setActive(combo.getActive());
        newCombo.setDescription(combo.getDescription());
        newCombo.setPrice(combo.getPrice());
        newCombo.setId(combo.getId());

        for (ProductCombo productCombo : combo.getProductCombos()) {
            Product product = productRepository.findById(productCombo.getProduct().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Product","id",productCombo.getProduct().getId()));
            newCombo.addProduct(product,productCombo.getQuantity());
        }

        return comboRepository.save(newCombo);
    }

    @GetMapping("/combos/{id}")
    public Combo getComboById(@PathVariable(value = "id") String comboId) {
        return comboRepository.findById(comboId)
                .orElseThrow(() -> new ResourceNotFoundException("Combo","id",comboId));
    }

    @PutMapping("/combos/{id}")
    public Combo updateCombo(@PathVariable(value = "id") String comboId,
                             @Valid @RequestBody Combo comboDetails) {
        Combo combo = comboRepository.findById(comboId)
                .orElseThrow(() -> new ResourceNotFoundException("Combo","id",comboId));

        combo.setDescription(comboDetails.getDescription());
        combo.setPrice(comboDetails.getPrice());
        combo.setName(comboDetails.getName());

        List<ProductCombo> productComboList = new ArrayList<>(combo.getProductCombos());

        for (ProductCombo productCombo : productComboList)
            combo.removeProduct(productCombo.getProduct());

        productComboRepository.deleteAll(productComboRepository.findByPrimaryKey_Combo_Id(combo.getId()));

        for (ProductCombo productCombo : productComboList) {
            Product newProduct = productRepository.findById(productCombo.getProduct().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Product","id",productCombo.getProduct().getId()));
            combo.addProduct(newProduct,productCombo.getQuantity());
        }
        Combo updatedCombo = comboRepository.save(combo);
        return updatedCombo;
    }

    @PatchMapping("/combos/{id}")
    public ResponseEntity<?> disableCombo(@PathVariable(value = "id") String comboId, Combo comboToDisable) {
        Combo combo = comboRepository.findById(comboId)
                .orElseThrow(() -> new ResourceNotFoundException("Combo","id",comboId));;

        combo.setActive(false);
        combo = comboRepository.save(combo);
        return ResponseEntity.ok(combo);
    }

    @PostMapping("/combos/bulk")
    public ResponseEntity<MassiveStorageResponse> bulkSave(@RequestBody List<Combo> combos) {
        int correctlyInserted = 0;
        int badlyInserted = 0;
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < combos.size(); i++) {
            Combo comb = combos.get(i);
            try {
                Optional<Combo> comboOptional = comboRepository.findById(comb.getId());
                if (comboOptional.isPresent())
                    throw new ResourceConflictException("Combo", comb.getId());

                Combo newCombo = new Combo();
                newCombo.setName(comb.getName());
                newCombo.setActive(comb.getActive());
                newCombo.setDescription(comb.getDescription());
                newCombo.setPrice(comb.getPrice());
                newCombo.setId(comb.getId());

                for (ProductCombo productCombo : comb.getProductCombos()) {
                    Product product = productRepository.findById(productCombo.getProduct().getId())
                            .orElseThrow(() -> new ResourceNotFoundException("Product", "id", productCombo.getProduct().getId()));
                    newCombo.addProduct(product, productCombo.getQuantity());
                }

                comboRepository.save(newCombo);
                correctlyInserted++;
            } catch (Exception ex) {
                badlyInserted++;
                positions.add(i);
            }
        }
        return ResponseEntity.ok(new MassiveStorageResponse(String.valueOf(correctlyInserted),
                String.valueOf(badlyInserted), positions));
    }

    @DeleteMapping("/combos/{id}")
    public ResponseEntity<String> deleteCombo(@PathVariable(value = "id") String comboId) {
        Combo combo = comboRepository.findById(comboId)
                .orElseThrow(() -> new ResourceNotFoundException("Combo","id",comboId));

        try {
            comboRepository.delete(combo);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/combos/{id}/products")
    public List<ProductCombo> getComboProducts(@PathVariable(value = "id") String comboId) {
        Combo combo = comboRepository.findById(comboId)
                .orElseThrow(() -> new ResourceNotFoundException("Combo","id",comboId));
        //return new ArrayList<>();
        return productComboRepository.findByPrimaryKey_Combo_Id(comboId);
    }
}
