package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.BadRequestException;
import com.dp1mtel.backend.exception.ResourceConflictException;
import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.*;
import com.dp1mtel.backend.repository.*;

import com.dp1mtel.backend.responses.MassiveStorageResponse;
import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    StoreRepository storeRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ComboRepository comboRepository;
    @Autowired
    InventoryOperationRepository inventoryOperationRepository;
    @Autowired
    InventoryOperationTypeRepository inventoryOperationTypeRepository;
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    LotRepository lotRepository;
    @Autowired
    OrderDetailRepository orderDetailRepository;
    @Autowired
    OrderStatusRepository orderStatusRepository;

    @GetMapping("")
    public List<Order> getAllOrders() { return orderRepository.findAll(); }

    @GetMapping("/{id}")
    public Order getOrderById(@PathVariable(value = "id") Long orderId) {
        return orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException("Order","id",orderId));
    }

    @PostMapping("")
    public Order createOrder(@Valid @RequestBody Order order) {

        //Crear nueva order
        Order newOrder = new Order();

        Customer customer = customerRepository.findById(order.getCustomer().getId())
                .orElseThrow(() -> new ResourceNotFoundException("Customer","id",order.getCustomer().getId()));

        User seller = userRepository.findById(order.getSeller().getId())
                .orElseThrow(() -> new ResourceNotFoundException("User","id",order.getSeller().getId()));

        Store store = storeRepository.findById(order.getStore().getId())
                .orElseThrow(() -> new ResourceNotFoundException("Store","id",order.getStore().getId()));

        OrderStatus orderStatus = orderStatusRepository.findBySlugEquals(order.getOrderStatus().getSlug())
                .orElseThrow(() -> new ResourceNotFoundException("OrderStatus","slug",order.getOrderStatus().getSlug()));

        newOrder.setCustomer(customer);
        newOrder.setSeller(seller);
        newOrder.setStore(store);
        newOrder.setCustomer(order.getCustomer());
        newOrder.setDeliveryAddress(order.getDeliveryAddress());
        newOrder.setRecipientDedication(order.getRecipientDedication());
        newOrder.setRecipientName(order.getRecipientName());
        newOrder.setOrderStatus(orderStatus);
        newOrder.setExpectedDeliveryDate(order.getExpectedDeliveryDate());
        newOrder.setUserReadyTime(order.getUserReadyTime());
        newOrder.setUserDueTime(order.getUserDueTime());
        newOrder.setReservationCode(order.getReservationCode());
        newOrder.setReservationTime(order.getReservationTime());
        newOrder.setCancellationReason(order.getCancellationReason());

        List<InventoryOperation> inventoryOperationsToSave = new ArrayList<>();

        List<Lot> inventory = lotRepository.findByStore_Id(store.getId());

        Double volume = 0.0;
        //Para cada producto o combo de la orden
        for (OrderDetail orderDetail : order.getOrderDetails()) {
            OrderDetail newOrderDetail = new OrderDetail();
            //if is product
            if ( orderDetail.getProduct() != null)  {
                Product product = productRepository.findById(orderDetail.getProduct().getId())
                        .orElseThrow(() -> new ResourceNotFoundException("Product","id",orderDetail.getProduct()));
                newOrderDetail.setProduct(product);
                newOrderDetail.setPrice(product.getPrice());
                newOrderDetail.setSubTotal(product.getPrice()*orderDetail.getQuantity()*(1-orderDetail.getDiscount()));
                newOrderDetail.setQuantity(orderDetail.getQuantity());

                //for each item in the product
                for (ProductItem productItem : product.getProductItems()) {
                    Item item = itemRepository.findById(productItem.getItem().getId())
                            .orElseThrow(() -> new ResourceNotFoundException("Item","id",productItem.getItem().getId()));

                    if (orderStatus.getSlug().equals(OrderStatus.OrderStatusEnum.COMPLETED.toString()) ||
                            orderStatus.getSlug().equals(OrderStatus.OrderStatusEnum.PAID.toString()))
                        inventoryOperationsToSave.addAll(getItemsFromLots(item,store,newOrder,newOrderDetail.getQuantity(),productItem,inventory));
                    volume = volume + productItem.getItem().getVolume()*productItem.getQuantity()*orderDetail.getQuantity();
                }

            //if is combo
            } else if ( orderDetail.getCombo() != null) {
                Combo combo = comboRepository.findById(orderDetail.getCombo().getId())
                        .orElseThrow(() -> new ResourceNotFoundException("Combo","id",orderDetail.getCombo().getId()));
                newOrderDetail.setCombo(combo);
                newOrderDetail.setPrice(combo.getPrice());
                newOrderDetail.setSubTotal(combo.getPrice()*orderDetail.getQuantity());
                newOrderDetail.setQuantity(orderDetail.getQuantity());

                for (ProductCombo productCombo : combo.getProductCombos()) {
                    Product product = productRepository.findById(productCombo.getProduct().getId())
                            .orElseThrow(() -> new ResourceNotFoundException("Product","id",productCombo.getProduct().getId()));

                    for (ProductItem productItem : product.getProductItems()) {
                        Item item = itemRepository.findById(productItem.getItem().getId())
                                .orElseThrow(() -> new ResourceNotFoundException("Item","id",productItem.getItem().getId()));
                        if (orderStatus.getSlug().equals(OrderStatus.OrderStatusEnum.COMPLETED.toString()) ||
                                orderStatus.getSlug().equals(OrderStatus.OrderStatusEnum.PAID.toString()))
                            inventoryOperationsToSave.addAll(getItemsFromLots(item,store,newOrder,newOrderDetail.getQuantity()*productCombo.getQuantity(),productItem,inventory));
                        volume = volume + productItem.getItem().getVolume()*productItem.getQuantity()*productCombo.getQuantity()*orderDetail.getQuantity();
                    }
                }
            }
            newOrderDetail.setDiscount(orderDetail.getDiscount());
            newOrder.addOrderDetail(newOrderDetail);
            newOrder.setVolume(volume);
        }
        if (orderStatus.getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
        } else {
            lotRepository.saveAll(inventory);
            inventoryOperationRepository.saveAll(inventoryOperationsToSave);
        }

        return orderRepository.save(newOrder);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteOrder(@PathVariable("id") Long orderId) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new ResourceNotFoundException("Order", "id", orderId));
        orderRepository.delete(order);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/bulk")
    public ResponseEntity<MassiveStorageResponse> bulkSave(@RequestBody List<Order> orders) {
        int correctlyInserted = 0;
        int badlyInserted = 0;
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++) {
            Order order = orders.get(i);
            try {

                Customer customer = customerRepository.findByDocument(order.getCustomer().getDocument())
                        .orElseThrow(() -> new ResourceNotFoundException("Customer","document",order.getCustomer().getDocument()));

                User seller = userRepository.findByDni(order.getSeller().getDni())
                        .orElseThrow(() -> new ResourceNotFoundException("User","dni",order.getSeller().getDni()));

                Store store = storeRepository.findByDistrict(order.getStore().getDistrict())
                        .orElseThrow(() -> new ResourceNotFoundException("Store","adress",order.getStore().getAddress()));

                OrderStatus orderStatus = orderStatusRepository.findBySlugEquals(order.getOrderStatus().getSlug())
                        .orElseThrow(() -> new ResourceNotFoundException("OrderStatus","slug",order.getOrderStatus().getSlug()));

                Order newOrder = new Order();
                newOrder.setCustomer(customer);
                newOrder.setSeller(seller);
                newOrder.setStore(store);

                UserAddress userAddress = addressRepository.findByCustomer_DocumentEquals(customer.getDocument()).get(0);

                newOrder.setDeliveryAddress(userAddress);
                newOrder.setRecipientDedication("Espero disfrutes este presente.");
                newOrder.setRecipientName("Persona especial");
                newOrder.setOrderStatus(orderStatus);
                newOrder.setExpectedDeliveryDate(order.getExpectedDeliveryDate());
                newOrder.setUserReadyTime(order.getUserReadyTime());
                newOrder.setUserDueTime(order.getUserDueTime());
//                newOrder.setReservationCode();
//                newOrder.setReservationTime();
//                newOrder.cancelation...

                List<InventoryOperation> inventoryOperationsToSave = new ArrayList<>();

                List<Lot> inventory = lotRepository.findByStore_Id(store.getId());

                Double volume = 0.0;

                for (OrderDetail orderDetail : order.getOrderDetails()) {
                    OrderDetail newOrderDetail = new OrderDetail();
                    //if is product
                    if ( orderDetail.getProduct() != null)  {
                        Product product = productRepository.findById(orderDetail.getProduct().getId())
                                .orElseThrow(() -> new ResourceNotFoundException("Product","id",orderDetail.getProduct()));
                        newOrderDetail.setProduct(product);
                        newOrderDetail.setPrice(product.getPrice());
                        newOrderDetail.setSubTotal(product.getPrice()*orderDetail.getQuantity()*(1-orderDetail.getDiscount()));
                        newOrderDetail.setQuantity(orderDetail.getQuantity());

                        //for each item in the product
                        for (ProductItem productItem : product.getProductItems()) {
                            Item item = itemRepository.findById(productItem.getItem().getId())
                                    .orElseThrow(() -> new ResourceNotFoundException("Item","id",productItem.getItem().getId()));


                                inventoryOperationsToSave.addAll(getItemsFromLots(item,store,newOrder,newOrderDetail.getQuantity(),productItem,inventory));
                            volume = volume + productItem.getItem().getVolume()*productItem.getQuantity()*orderDetail.getQuantity();
                        }

                        //if is combo
                    } else if ( orderDetail.getCombo() != null) {
                        Combo combo = comboRepository.findById(orderDetail.getCombo().getId())
                                .orElseThrow(() -> new ResourceNotFoundException("Combo","id",orderDetail.getCombo().getId()));
                        newOrderDetail.setCombo(combo);
                        newOrderDetail.setPrice(combo.getPrice());
                        newOrderDetail.setSubTotal(combo.getPrice()*orderDetail.getQuantity());
                        newOrderDetail.setQuantity(orderDetail.getQuantity());

                        for (ProductCombo productCombo : combo.getProductCombos()) {
                            Product product = productRepository.findById(productCombo.getProduct().getId())
                                    .orElseThrow(() -> new ResourceNotFoundException("Product","id",productCombo.getProduct().getId()));

                            for (ProductItem productItem : product.getProductItems()) {
                                Item item = itemRepository.findById(productItem.getItem().getId())
                                        .orElseThrow(() -> new ResourceNotFoundException("Item","id",productItem.getItem().getId()));

                                    inventoryOperationsToSave.addAll(getItemsFromLots(item,store,newOrder,newOrderDetail.getQuantity()*productCombo.getQuantity(),productItem,inventory));
                                volume = volume + productItem.getItem().getVolume()*productItem.getQuantity()*productCombo.getQuantity()*orderDetail.getQuantity();
                            }
                        }
                    }
                    newOrderDetail.setDiscount(orderDetail.getDiscount());
                    newOrder.addOrderDetail(newOrderDetail);
                    newOrder.setVolume(volume);
                }

                    lotRepository.saveAll(inventory);
                    inventoryOperationRepository.saveAll(inventoryOperationsToSave);
                


                orderRepository.save(newOrder);
                correctlyInserted++;

            } catch (Exception ex) {
                badlyInserted++;
                positions.add(i);
            }
        }
        return ResponseEntity.ok(new MassiveStorageResponse(String.valueOf(correctlyInserted),
                String.valueOf(badlyInserted), positions));
    }

    private Integer getLotIndex(List<Lot> lotList,Long lotId) {
        Integer index = -1;
        Integer i = 0;
        for (Lot lot : lotList) {
            if (lot.getId().equals(lotId))
                index = i;
            i++;
        }
        return index;
    }

    private List<InventoryOperation> returnItemsToLots(Item item,
                                                       Store store,
                                                       Order order,
                                                       Integer quantity,
                                                       List<Lot> inventory) {

        List<InventoryOperation> operationsToSave = new ArrayList<>();

        List<Lot> lotList = inventory.stream()
                .filter( lot ->  lot.getItem().getId().equals(item.getId()) )
                .collect(Collectors.toList());
        if (item.getExpires()) {
            lotList.sort(new Comparator<Lot>() {
                @Override
                public int compare(Lot lot, Lot t1) {
                    return lot.getExpirationDate().compareTo(t1.getExpirationDate());
                }
            });
        } else {
            lotList.sort(new Comparator<Lot>() {
                @Override
                public int compare(Lot lot, Lot t1) {
                    return lot.getCreatedAt().compareTo(t1.getCreatedAt());
                }
            });
        }

        List<InventoryOperation> operationList =
                inventoryOperationRepository.findByStore_IdAndItem_IdAndInventoryOperationType_Name(store.getId()
                        ,item.getId(), "Compra");

        Integer itemsToReturn = quantity;

        List<Lot> lotsToModify = new ArrayList<>();
        Lot lot;

        do {
            lot = lotList.remove(0);
            Lot finalLot = lot;
            Integer initialQuantity = operationList.stream()
                    .filter( op -> op.getLot().getId().equals(finalLot.getId()))
                    .collect(Collectors.toList()).get(0).getQuantity();
            Integer quantityToReturn = (itemsToReturn>lot.getQuantity())? initialQuantity: itemsToReturn;
            lot.setQuantity(lot.getQuantity() + quantityToReturn);

            InventoryOperation newInventoryOperation = new InventoryOperation();
            newInventoryOperation.setItem(item);
            newInventoryOperation.setInventoryOperationType(
                    inventoryOperationTypeRepository.findByNameEquals("Devolucion").get(0));
            newInventoryOperation.setQuantity(quantityToReturn);
            newInventoryOperation.setDescription("Devolucion de Items");
            newInventoryOperation.setLot(lot);
            newInventoryOperation.setStore(store);
            newInventoryOperation.setUser(order.getSeller());
            operationsToSave.add(newInventoryOperation);
            lotsToModify.add(lot);
            itemsToReturn = itemsToReturn - quantityToReturn;
        } while(itemsToReturn > 0);

        for (Lot lotToModify : lotsToModify) {
            Integer index = getLotIndex(inventory,lotToModify.getId());
            inventory.get(index).setQuantity(lotToModify.getQuantity());
        }

        return operationsToSave;
    }

    private List<InventoryOperation> getItemsFromLots(Item item,Store store,Order newOrder,Integer quantity
            , ProductItem productItem, List<Lot> inventory) {

        List<Lot> lotList = inventory.stream()
                .filter( lot ->  lot.getItem().getId().equals(item.getId()) )
                .collect(Collectors.toList());

        if (item.getExpires()) {
            lotList.sort(new Comparator<Lot>() {
                @Override
                public int compare(Lot lot, Lot t1) {
                    return lot.getExpirationDate().compareTo(t1.getExpirationDate());
                }
            });
        } else {
            lotList.sort(new Comparator<Lot>() {
                @Override
                public int compare(Lot lot, Lot t1) {
                    return lot.getCreatedAt().compareTo(t1.getCreatedAt());
                }
            });
        }

        Lot lot;

        List<Lot> lotsToModify = new ArrayList<>();

        Integer itemsLeft = productItem.getQuantity()*quantity;

        List<InventoryOperation> operationsToSave = new ArrayList<>();

        do {
            if (lotList.size() > 0) {

                if (lotList.get(0).getQuantity() >= itemsLeft ) {
                    lot = lotList.remove(0);
                    InventoryOperation inventoryOperation = new InventoryOperation();
                    inventoryOperation.setStore(newOrder.getStore());
                    inventoryOperation.setDescription(newOrder.orderDetailsLog());
                    inventoryOperation.setItem(item);
                    inventoryOperation.setLot(lot);
                    inventoryOperation.setUser(newOrder.getSeller());
                    inventoryOperation.setInventoryOperationType(
                            inventoryOperationTypeRepository.findByNameEquals("Venta").get(0)
                    );

                    Integer quantityToGet = (itemsLeft > lot.getQuantity())? lot.getQuantity():itemsLeft;
                    lot.setQuantity(lot.getQuantity() - quantityToGet);
                    inventoryOperation.setQuantity(quantityToGet);
                    itemsLeft = itemsLeft - quantityToGet;
                    operationsToSave.add(inventoryOperation);
                    lotsToModify.add(lot);
                } else {
                    throw new BadRequestException();
                }
            }
            else {
                throw new BadRequestException();
            }
        } while (itemsLeft > 0);

        for (Lot lotToModify : lotsToModify) {
             Integer index = getLotIndex(inventory,lotToModify.getId());
             inventory.get(index).setQuantity(lotToModify.getQuantity());
        }

        return operationsToSave;
    }

    private Integer findOrderDetail(List<OrderDetail> orderDetails, Product product, Combo combo) {
        Integer index = -1;
        Integer i = 0;
        for (OrderDetail orderDetail : orderDetails) {
            if (orderDetail.getCombo() != null && combo != null && orderDetail.getCombo().getId().equals(combo.getId()) )
                index = i;
            else if (orderDetail.getProduct() != null && product != null && orderDetail.getProduct().getId().equals(product.getId()) )
                index = i;
            i++;
        }

        return index;
    }

    private List<OrderDetail> inFirstBuNotInSecond(List<OrderDetail> firstOrderList, List<OrderDetail> secondOrderList) {
        List<OrderDetail> list = new ArrayList<>();

        for (OrderDetail orderDetail : firstOrderList) {
            if (findOrderDetail(secondOrderList,orderDetail.getProduct(),orderDetail.getCombo()) == -1)
                list.add(orderDetail);
        }
        return list;
    }

    @PutMapping("/{id}")
    public Order updateOrder(@PathVariable(value = "id") Long orderId, @RequestBody Order newOrder) {

        Order previousOrder = orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException("Order","id",orderId));

        Store store = storeRepository.findById(previousOrder.getStore().getId())
                .orElseThrow(() -> new ResourceNotFoundException("Store","id",previousOrder.getStore().getId()));

        //Put null if id 0
        for (OrderDetail orderDetail : newOrder.getOrderDetails())
            if (orderDetail.getId() == 0)
                orderDetail.setId(null);


        List<OrderDetail> inOldButNotInNew = inFirstBuNotInSecond(previousOrder.getOrderDetails(),newOrder.getOrderDetails());
        List<OrderDetail> inNewButNotInOld = inFirstBuNotInSecond(newOrder.getOrderDetails(),previousOrder.getOrderDetails());

        List<InventoryOperation> inventoryOperationsToSave = new ArrayList<>();

        List<Lot> inventory = lotRepository.findByStore_Id(store.getId());

        if (newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.CANCELED.toString())) {
            OrderStatus orderStatus = orderStatusRepository.findBySlugEquals(OrderStatus.OrderStatusEnum.CANCELED.toString())
                    .orElseThrow(() -> new ResourceNotFoundException("OrderStatus","slug",newOrder.getOrderStatus().getSlug()));
            newOrder.setOrderStatus(orderStatus);
            return orderRepository.save(newOrder);
        }

        if (previousOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString()) && (
                newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.COMPLETED.toString()) ||
                newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.PAID.toString()))
                ) {
            for (OrderDetail orderDetail:previousOrder.getOrderDetails()) {
                if ( orderDetail.getProduct() != null)  {
                    Product product = productRepository.findById(orderDetail.getProduct().getId())
                            .orElseThrow(() -> new ResourceNotFoundException("Product","id",orderDetail.getProduct()));


                    //for each item in the product
                    for (ProductItem productItem : product.getProductItems()) {
                        Item item = itemRepository.findById(productItem.getItem().getId())
                                .orElseThrow(() -> new ResourceNotFoundException("Item","id",productItem.getItem().getId()));

                        inventoryOperationsToSave.addAll(getItemsFromLots(item,store,newOrder,orderDetail.getQuantity(),productItem,inventory));
                    }

                    //if is combo
                } else if ( orderDetail.getCombo() != null) {
                    Combo combo = comboRepository.findById(orderDetail.getCombo().getId())
                            .orElseThrow(() -> new ResourceNotFoundException("Combo","id",orderDetail.getCombo().getId()));

                    for (ProductCombo productCombo : combo.getProductCombos()) {
                        Product product = productRepository.findById(productCombo.getProduct().getId())
                                .orElseThrow(() -> new ResourceNotFoundException("Product","id",productCombo.getProduct().getId()));

                        for (ProductItem productItem : product.getProductItems()) {
                            Item item = itemRepository.findById(productItem.getItem().getId())
                                    .orElseThrow(() -> new ResourceNotFoundException("Item","id",productItem.getItem().getId()));

                            inventoryOperationsToSave.addAll(getItemsFromLots(item,store,newOrder,orderDetail.getQuantity()*productCombo.getQuantity(),productItem, inventory));
                        }
                    }
                }
            }
            inventoryOperationRepository.saveAll(inventoryOperationsToSave);
            lotRepository.saveAll(inventory);
            return orderRepository.save(previousOrder);
        }

        //Remove orderDetail from order, register InvMovements, add quantity to lots
        for (OrderDetail orderDetail : inOldButNotInNew) {
            previousOrder.removeOrderDetail(orderDetail);

            if (orderDetail.getProduct() != null) {
                for (ProductItem productItem:orderDetail.getProduct().getProductItems()) {
                    if (!newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
                        inventoryOperationsToSave.addAll(returnItemsToLots(productItem.getItem(), store, previousOrder,
                                productItem.getQuantity() * orderDetail.getQuantity(), inventory));
                    }
                }

            } else if (orderDetail.getCombo() != null) {
                for (ProductCombo productCombo : orderDetail.getCombo().getProductCombos()) {
                    for (ProductItem productItem : productCombo.getProduct().getProductItems()) {
                        if (!newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
                            inventoryOperationsToSave.addAll(returnItemsToLots(productItem.getItem(), store, previousOrder,
                                    productItem.getQuantity() * orderDetail.getQuantity() * productCombo.getQuantity(),
                                    inventory));
                        }
                    }
                }
            }
        }


        //Add new Order Detail as in POST
        for (OrderDetail orderDetail : inNewButNotInOld) {
            previousOrder.addOrderDetail(orderDetail);
        }

        for (OrderDetail orderDetail : inNewButNotInOld ){
            OrderDetail newOrderDetail = new OrderDetail();
            //if is product
            if ( orderDetail.getProduct() != null)  {
                Product product = productRepository.findById(orderDetail.getProduct().getId())
                        .orElseThrow(() -> new ResourceNotFoundException("Product","id",orderDetail.getProduct()));
                newOrderDetail.setProduct(product);
                newOrderDetail.setPrice(product.getPrice());
                newOrderDetail.setSubTotal(product.getPrice()*orderDetail.getQuantity()*(1-orderDetail.getDiscount()));
                newOrderDetail.setQuantity(orderDetail.getQuantity());

                //for each item in the product
                for (ProductItem productItem : product.getProductItems()) {
                    Item item = itemRepository.findById(productItem.getItem().getId())
                            .orElseThrow(() -> new ResourceNotFoundException("Item","id",productItem.getItem().getId()));
                    if (!newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
                        inventoryOperationsToSave.addAll(getItemsFromLots(item, store, newOrder, newOrderDetail.getQuantity(), productItem, inventory));
                    }
                }

                //if is combo
            } else if ( orderDetail.getCombo() != null) {
                Combo combo = comboRepository.findById(orderDetail.getCombo().getId())
                        .orElseThrow(() -> new ResourceNotFoundException("Combo","id",orderDetail.getCombo().getId()));
                newOrderDetail.setCombo(combo);
                newOrderDetail.setPrice(combo.getPrice());
                newOrderDetail.setSubTotal(combo.getPrice()*orderDetail.getQuantity());
                newOrderDetail.setQuantity(orderDetail.getQuantity());

                for (ProductCombo productCombo : combo.getProductCombos()) {
                    Product product = productRepository.findById(productCombo.getProduct().getId())
                            .orElseThrow(() -> new ResourceNotFoundException("Product","id",productCombo.getProduct().getId()));

                    for (ProductItem productItem : product.getProductItems()) {
                        Item item = itemRepository.findById(productItem.getItem().getId())
                                .orElseThrow(() -> new ResourceNotFoundException("Item","id",productItem.getItem().getId()));
                        if (!newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
                            inventoryOperationsToSave.addAll(getItemsFromLots(item, store, newOrder, newOrderDetail.getQuantity() * productCombo.getQuantity(), productItem, inventory));
                        }
                    }
                }
            }
            newOrderDetail.setDiscount(orderDetail.getDiscount());
            previousOrder.addOrderDetail(newOrderDetail);
        }

        //For each order detail that remained the "same", register inventoryMovements, discount or add to lots
        for (OrderDetail orderDetail : previousOrder.getOrderDetails()) {
            OrderDetail newOrderDetail = newOrder.findOrderDetail(orderDetail.getProduct(),orderDetail.getCombo());

            if (orderDetail.getQuantity() > newOrderDetail.getQuantity()) {
                if (orderDetail.getProduct() != null) {
                    for (ProductItem productItem : orderDetail.getProduct().getProductItems()) {
                        if (!newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
                            inventoryOperationsToSave.addAll(returnItemsToLots(productItem.getItem(), store, newOrder,
                                    (orderDetail.getQuantity() - newOrderDetail.getQuantity()) * productItem.getQuantity(),
                                    inventory));
                        }
                    }
                } else if ( orderDetail.getCombo() != null) {
                    for ( ProductCombo productCombo : orderDetail.getCombo().getProductCombos()) {
                        for (ProductItem productItem : productCombo.getProduct().getProductItems()) {
                            if (!newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
                                inventoryOperationsToSave.addAll(returnItemsToLots(productItem.getItem(), store, newOrder,
                                        (orderDetail.getQuantity() - newOrderDetail.getQuantity()) * productCombo.getQuantity() * productItem.getQuantity(),
                                        inventory));
                            }
                        }
                    }
                }


            } else if (orderDetail.getQuantity() < newOrderDetail.getQuantity()) {
                if (orderDetail.getProduct() != null) {
                    for (ProductItem productItem : orderDetail.getProduct().getProductItems()) {
                        if (!newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
                            inventoryOperationsToSave.addAll(getItemsFromLots(productItem.getItem(), store, newOrder,
                                    (newOrderDetail.getQuantity() - orderDetail.getQuantity())
                                    , productItem, inventory));
                        }
                    }
                } else if ( orderDetail.getCombo() != null) {
                    for ( ProductCombo productCombo : orderDetail.getCombo().getProductCombos()) {
                        for (ProductItem productItem : productCombo.getProduct().getProductItems()) {
                            if (!newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
                                inventoryOperationsToSave.addAll(getItemsFromLots(productItem.getItem(), store, newOrder,
                                        (newOrderDetail.getQuantity() - orderDetail.getQuantity()) * productCombo.getQuantity(),
                                        productItem, inventory));
                            }
                        }
                    }
                }
            } else {
                continue;
            }

            orderDetail.setQuantity(newOrderDetail.getQuantity());
            orderDetail.setSubTotal(orderDetail.getPrice()*newOrderDetail.getQuantity());
        }
        if (newOrder.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())){
            inventory.clear();
            inventoryOperationsToSave.clear();
        } else {
            inventoryOperationRepository.saveAll(inventoryOperationsToSave);
            lotRepository.saveAll(inventory);
        }

        previousOrder.setDeliveryAddress(newOrder.getDeliveryAddress());
        previousOrder.setExpectedDeliveryDate(newOrder.getExpectedDeliveryDate());
        previousOrder.setUserReadyTime(newOrder.getUserReadyTime());
        previousOrder.setUserDueTime(newOrder.getUserDueTime());

        return orderRepository.save(previousOrder);
    }

    @PostMapping("/reserve")
    public Order reserveOrder(@RequestBody Order order) {
//        // If order already has a status, it cannot be reserved
        if (!order.getOrderStatus().getSlug().equals(OrderStatus.OrderStatusEnum.RESERVED.toString())) {
            throw new BadRequestException("Pedido no puede ser reservado");
        }

        Hashids hashids = new Hashids(order.getCustomer().getDocument() + order.getSeller().getDni());
        order.setReservationCode(hashids.encode(ThreadLocalRandom.current().nextLong(Hashids.MAX_NUMBER)));
        order.setReservationTime(new Date());
        return createOrder(order);
    }

    @PostMapping("/mobile")
    public Order saveOrderMobile(@RequestBody Order order) {
        User seller = userRepository.findAll().get(0);
        order.setSeller(seller);
        order.setStore(seller.getStore());
        return createOrder(order);
    }

    @PostMapping("/{id}/refund")
    public Order refundOrder(@RequestBody Order order) {
        OrderStatus refundStatus = orderStatusRepository
                .findBySlugEquals(OrderStatus.OrderStatusEnum.RETURNED.toString())
                .orElseThrow(() -> new ResourceNotFoundException(
                        "OrderStatus", "slug", OrderStatus.OrderStatusEnum.RETURNED.toString()));
        Store store = storeRepository.findById(order.getStore().getId())
                .orElseThrow(() -> new ResourceNotFoundException("Store", "id", order.getStore().getId()));
        Order newOrder = orderRepository.findById(order.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Order", "id", order.getId()));
        newOrder.setOrderStatus(refundStatus);
        List<Lot> inventory = lotRepository.findByStore_Id(store.getId());
        List<InventoryOperation> operations = new ArrayList<>();
        for (OrderDetail orderDetail : newOrder.getOrderDetails()) {
            operations.addAll(returnOrderDetailToLots(orderDetail, store, newOrder, inventory));
        }
        inventoryOperationRepository.saveAll(operations);
        orderRepository.save(newOrder);
        return newOrder;
    }

    private Collection<InventoryOperation> returnOrderDetailToLots(OrderDetail orderDetail,
                                                                   Store store,
                                                                   Order order,
                                                                   List<Lot> inventory) {
        List<InventoryOperation> operations = new ArrayList<>();
        if (orderDetail.getProduct() != null) {
            for (ProductItem productItem : orderDetail.getProduct().getProductItems()) {
                operations.addAll(returnItemsToLots(
                        productItem.getItem(),
                        store,
                        order,
                        orderDetail.getQuantity() * productItem.getQuantity(),
                        inventory));
            }
        } else if (orderDetail.getCombo() != null) {
            for (ProductCombo productCombo : orderDetail.getCombo().getProductCombos()) {
                for (ProductItem productItem : productCombo.getProduct().getProductItems()) {
                    operations.addAll(returnItemsToLots(
                            productItem.getItem(),
                            store,
                            order,
                            orderDetail.getQuantity() * productCombo.getQuantity() * productItem.getQuantity(),
                            inventory));
                }
            }
        }
        return operations;
    }

    @PostMapping("/{id}/pay")
    public ResponseEntity<Order> confirmPayment(@RequestBody Order oldOrder,
                                                @RequestParam("reservation_code") String reservationCode) {
        if (reservationCode == null) {
            return ResponseEntity.status(401).build();
        }
        Order order = orderRepository.findById(oldOrder.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Order", "id", reservationCode));
        if (!reservationCode.equals(order.getReservationCode())) {
            return ResponseEntity.status(400).build();
        }
        OrderStatus status;
        if (order.getDeliveryAddress() == null) {
             status = orderStatusRepository.findBySlugEquals(OrderStatus.OrderStatusEnum.COMPLETED.toString())
                    .orElseThrow(() -> new ResourceNotFoundException("OrderStatus", "slug", OrderStatus.OrderStatusEnum.COMPLETED.toString()));
        } else {
            status = orderStatusRepository.findBySlugEquals(OrderStatus.OrderStatusEnum.PAID.toString())
                    .orElseThrow(() -> new ResourceNotFoundException("OrderStatus", "slug", OrderStatus.OrderStatusEnum.PAID.toString()));
        }
        // Should validate bank operation
        order.setOrderStatus(status);
        return ResponseEntity.ok(updateOrder(order.getId(), order));
    }
}