package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.ProductCategoryDiscount;
import com.dp1mtel.backend.repository.ProductCategoryDiscountRepository;
import com.vividsolutions.jts.index.bintree.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/product-category-discounts")
public class ProductCategoryDiscountController {

    @Autowired
    ProductCategoryDiscountRepository productCategoryDiscountRepository;

    @GetMapping("")
    public List<ProductCategoryDiscount> getAllProductCategoryDiscounts() { return productCategoryDiscountRepository.findAll(); }

    @GetMapping("/{id}")
    public ProductCategoryDiscount getProductCategoryDiscountById(@PathVariable(value = "id") Long productCategoryDiscountId) {
        return productCategoryDiscountRepository.findById(productCategoryDiscountId)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategoryDiscount","id",productCategoryDiscountId));
    }

    @PostMapping("")
    public ResponseEntity<?> createProductCategoryDiscount(@Valid @RequestBody ProductCategoryDiscount productCategoryDiscount) {
        List<ProductCategoryDiscount> list = productCategoryDiscountRepository.findByProductCategory_Id(productCategoryDiscount.getProductCategory().getId());

        Date startDate = productCategoryDiscount.getStartDate();
        Date endDate = productCategoryDiscount.getEndDate();

        Boolean repeated = false;

        for (int i = 0; i < list.size();i++){
            Interval interval = new Interval(list.get(i).getStartDate().getTime(),list.get(i).getEndDate().getTime());
            if (interval.contains(startDate.getTime()) || interval.contains(endDate.getTime())){
                repeated = true;
                break;
            }
        }

        if (repeated)
            return ResponseEntity.badRequest().build();

        ProductCategoryDiscount updatedProductCategoryDiscount = productCategoryDiscountRepository.save(productCategoryDiscount);

        return ResponseEntity.ok(updatedProductCategoryDiscount);
    }

    @PutMapping("/{id}")
    public ProductCategoryDiscount updateProductCategoryDiscount(@PathVariable(value = "id") Long prodCatDiscId, @Valid @RequestBody ProductCategoryDiscount productCategoryDiscount) {
        ProductCategoryDiscount oldProductCategoryDiscount = productCategoryDiscountRepository.findById(prodCatDiscId)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategoryDiscount","id",prodCatDiscId));
        oldProductCategoryDiscount.setActive(productCategoryDiscount.getActive());

        return productCategoryDiscountRepository.save(oldProductCategoryDiscount);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProductCategoryDiscount(@PathVariable(value = "id") Long prodCatDiscId, @PathVariable("id") String id){
        ProductCategoryDiscount productCategoryDiscount = productCategoryDiscountRepository.findById(prodCatDiscId)
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategoryDiscount","id",prodCatDiscId));

        try {
            productCategoryDiscountRepository.delete(productCategoryDiscount);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().build();
    }
}
