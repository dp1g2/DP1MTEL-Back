package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.Combo;
import com.dp1mtel.backend.model.QuantityDiscount;
import com.dp1mtel.backend.repository.ComboRepository;
import com.dp1mtel.backend.repository.QuantityDiscountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/quantity-discounts")
public class QuantityDiscountController {
    @Autowired
    QuantityDiscountRepository quantityDiscountRepository;
    @Autowired
    ComboController comboController;
    @Autowired
    ComboRepository comboRepository;

    @GetMapping("")
    public List<QuantityDiscount> getAllQuantityDiscounts() {
        return quantityDiscountRepository.findAll();
    }

    @GetMapping("/{id}")
    public QuantityDiscount getQuantityDiscountById(@PathVariable(value = "id") Long quantityDiscountId) {
        return quantityDiscountRepository.findById(quantityDiscountId)
                .orElseThrow(() -> new ResourceNotFoundException("QuantityDiscount","id",quantityDiscountId));
    }
    @PostMapping("")
    public ResponseEntity<?> createQuantityDiscount(@Valid @RequestBody QuantityDiscount quantityDiscount) {

        List<QuantityDiscount> quantityDiscountList = quantityDiscountRepository.findAll().
                stream().filter(qD -> {
            return qD.getCombo().getActive();
        }).collect(Collectors.toList());

        for (QuantityDiscount quantityDiscount1 : quantityDiscountList) {
            if (quantityDiscount1.getCombo().getProductCombos().get(0).getProduct().getId()
                    .equals(quantityDiscount.getCombo().getProductCombos().get(0).getProduct().getId())  &&
                    quantityDiscount1.getCombo().getProductCombos().get(0).getQuantity()
                            .equals(quantityDiscount.getCombo().getProductCombos().get(0).getQuantity())) {
                if ((!quantityDiscount.getStartDate().before(quantityDiscount1.getStartDate()) &&
                        !quantityDiscount.getStartDate().after(quantityDiscount1.getStartDate())) ||
                        (!quantityDiscount.getEndDate().before(quantityDiscount1.getStartDate()) &&
                                !quantityDiscount.getEndDate().after(quantityDiscount1.getStartDate()))){
                    return ResponseEntity.badRequest().build();
                }
            }

        }

        Combo combo = comboController.createCombo(quantityDiscount.getCombo());

        if (quantityDiscount.getStartDate().before(new Date()))
            combo.setActive(false);

        quantityDiscount.setCombo(combo);
        return ResponseEntity.ok(quantityDiscountRepository.save(quantityDiscount));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteQuantityDiscount(@PathVariable(value = "id") Long quantityDiscountId) {
        QuantityDiscount quantityDiscount = quantityDiscountRepository.findById(quantityDiscountId)
                .orElseThrow(() -> new ResourceNotFoundException("QuantityDiscount","id",quantityDiscountId));

        Combo combo = quantityDiscount.getCombo();
        combo.setActive(false);


        try {
            quantityDiscountRepository.delete(quantityDiscount);
            comboRepository.save(combo);
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }


}
