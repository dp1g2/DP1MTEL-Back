package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceConflictException;
import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.Role;
import com.dp1mtel.backend.model.Store;
import com.dp1mtel.backend.model.User;
import com.dp1mtel.backend.repository.RoleRepository;
import com.dp1mtel.backend.repository.StoreRepository;
import com.dp1mtel.backend.repository.UserRepository;
import com.dp1mtel.backend.responses.MassiveStorageResponse;
import com.dp1mtel.backend.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.*;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.CompletableFuture;



@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    StoreRepository storeRepository;

    @Autowired
    private EmailService emailService;


    @GetMapping("")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }


    @PostMapping("")
    public User createUser(@Valid @RequestBody User user) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        user.generatePassword();
        return userRepository.save(user);
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable(value = "id") Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable(value = "id") Long userId,
                           @Valid @RequestBody User userDetails) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));

        user.setActive(userDetails.getActive());
        user.setEmail(userDetails.getEmail());
        user.setLastName(userDetails.getLastName());
        user.setFirstName(userDetails.getFirstName());
        user.setGender(userDetails.getGender());

        if (userDetails.getResetToken() == null) userDetails.setResetToken("no-token");
        user.setResetToken(userDetails.getResetToken());

        User updatedUser = userRepository.save(user);
        return updatedUser;
    }

    @PostMapping("/bulk")
    public ResponseEntity<MassiveStorageResponse> bulkSave(@RequestBody List<User> users) {
        int correctlyInserted = 0;
        int badlyInserted = 0;
        String tokenTest = "";
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            try {
                //Find by DNI
                Optional<User> userOptional = userRepository.findByDni(user.getDni());
                Optional<Store> storeOptional = storeRepository.findByDistrict(user.getStore().getDistrict());

                if (userOptional.isPresent())
                    throw new ResourceConflictException("User", user.getDni());

                if (storeOptional.isPresent())
                    System.out.println("Realmente existe el distrito" + storeOptional.get().getAddress());

                User newUser = new User();
                newUser.setFirstName(user.getFirstName());
                newUser.setLastName(user.getLastName());
                newUser.setPassword("password");
                newUser.generatePassword();
                newUser.setActive(true);
                newUser.setBirthday(user.getBirthday());
                newUser.setDni(user.getDni());
                newUser.setEmail(user.getEmail());
                newUser.setGender(user.getGender());
                newUser.setRole(user.getRole());
                newUser.setStore(storeOptional.get());

                Role roleInUser = user.getRole();

                Optional<Role> roleOptional = roleRepository.findByNameEquals(roleInUser.getName());

                if (!roleOptional.isPresent())
                    throw new ResourceConflictException("Role", roleInUser.getName());

                newUser.setRole(roleOptional.get());
                newUser.setResetToken(UUID.randomUUID().toString());
                tokenTest = newUser.getResetToken();

                //Send email to the corresponding person to change email
                SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
                passwordResetEmail.setFrom("margaritatel.team@gmail.com");
                passwordResetEmail.setTo(user.getEmail());
                passwordResetEmail.setSubject("Password Reset Request");
                passwordResetEmail.setText("Por favor, ingrese al siguiente link para reestablecer su contrasena. " +
                        "http://200.16.7.147/reset?token=" + tokenTest);

                CompletableFuture.supplyAsync(() -> {
                    emailService.sendEmail(passwordResetEmail);
                    return null;
                });
                userRepository.save(newUser);

                correctlyInserted++;

            } catch (Exception ex) {
                ex.printStackTrace();
                badlyInserted++;
                positions.add(i);
            }
        }
        return ResponseEntity.ok(new MassiveStorageResponse(String.valueOf(correctlyInserted),
                String.valueOf(badlyInserted), positions));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));

        userRepository.delete(user);

        return ResponseEntity.ok().build();
    }

    private static void sendFromGMail(String from, String pass, String[] to, String subject, String body) {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.user", from);
        props.put("mail.smtp.password", pass);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(from));
            InternetAddress[] toAddress = new InternetAddress[to.length];

            // To get the array of addresses
            for (int i = 0; i < to.length; i++) {
                toAddress[i] = new InternetAddress(to[i]);
            }

            for (int i = 0; i < toAddress.length; i++) {
                message.addRecipient(Message.RecipientType.TO, toAddress[i]);
            }

            message.setSubject(subject);
            message.setText(body);
            Transport transport = session.getTransport("smtp");
            transport.connect(host, from, pass);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }

}
