package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.*;
import com.dp1mtel.backend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class StoreController {

    @Autowired
    StoreRepository storeRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    InventoryOperationRepository inventoryOperationRepository;
    @Autowired
    LotRepository lotRepository;
    @Autowired
    DistrictRepository districtRepository;

    @GetMapping("/stores")
    public List<Store> getAllStores() {
        return storeRepository.findAll();
    }

    @PostMapping("/stores")
    public Store createStores(@Valid @RequestBody Store store) {
        Store newStore = new Store();
        newStore.setActive(store.getActive());
        newStore.setAddress(store.getAddress());
        newStore.setNumVehicles(store.getNumVehicles());
        newStore.setVehiclesCapacity(store.getVehiclesCapacity());
        newStore.setDistrict(store.getDistrict());
        newStore.setCoordinates(store.getCoordinates());

        newStore = storeRepository.save(newStore);
        for(User user : store.getUsers()) {
            User newUser = userRepository.findById(user.getId())
                    .orElseThrow(() -> new ResourceNotFoundException("User","id",user.getId()));
            newUser.setStore(newStore);
            userRepository.save(newUser);
            //store.addUser(newUser);
        }
        newStore.setNumEmployees(newStore.getUsers().size());
        return storeRepository.save(newStore);
    }

    @GetMapping("/stores/{id}")
    public Store getStoreById(@PathVariable(value = "id") Long storeId) {
        return storeRepository.findById(storeId)
                .orElseThrow(() -> new ResourceNotFoundException("Store", "id", storeId));
    }

    @PutMapping("/stores/{id}")
    public Store updateStore(@PathVariable(value = "id") Long storeId,
                       @Valid @RequestBody Store storeDetails) {
        Store store = storeRepository.findById(storeId)
                .orElseThrow(()-> new ResourceNotFoundException("Store", "id", storeId));

        store.setAddress(storeDetails.getAddress());
        store.setDistrict(storeDetails.getDistrict());
        store.setActive(storeDetails.getActive());
        store.setNumVehicles(storeDetails.getNumVehicles());
        store.setVehiclesCapacity(storeDetails.getVehiclesCapacity());

        List<User> newUserList = new ArrayList<>();

        Store updatedStore = storeRepository.save(store);
        for(User user : storeDetails.getUsers()) {
            User newUser = userRepository.findById(user.getId())
                    .orElseThrow(() -> new ResourceNotFoundException("User","id",user.getId()));
            newUser.setStore(updatedStore);
            newUserList.add(newUser);
        }
        store.setNumEmployees(store.getUsers().size());

        userRepository.saveAll(newUserList);
        return updatedStore;
    }

    @DeleteMapping("/stores/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(value = "id") Long storeId) {
        Store store = storeRepository.findById(storeId)
                .orElseThrow(() -> new ResourceNotFoundException("User","id", storeId));

        storeRepository.delete(store);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/stores/{id}/users")
    public List<User> getStoreUsers(@PathVariable(value = "id") Long storeId) {
        Store store = storeRepository.findById(storeId)
                .orElseThrow(() -> new ResourceNotFoundException("User","id", storeId));
        return userRepository.findByStoreId(storeId);
    }

    @PutMapping("/stores/{id}/users")
    public List<User> updatedStoreUsers(@PathVariable(value = "id") Long storeId, @RequestBody List<User> users) {
        Store store = storeRepository.findById(storeId)
                .orElseThrow(()-> new ResourceNotFoundException("Store", "id", storeId));
        List<User> newUserList = new ArrayList<>();

        for (User user : store.getUsers() ) {
            user.setStore(null);
            userRepository.save(user);
        }

        Store updatedStore = storeRepository.save(store);
        for(User user : users) {
            User newUser = userRepository.findById(user.getId())
                    .orElseThrow(() -> new ResourceNotFoundException("User","id",user.getId()));
            newUser.setStore(updatedStore);
            newUserList.add(newUser);
        }
        store.setNumEmployees(newUserList.size());

        userRepository.saveAll(newUserList);
        return newUserList;
    }


    @GetMapping("/stores/{id}/inventory-operations")
    public List<InventoryOperation> getStoreInventoryOperations(@PathVariable(value = "id") Long storeId) {
        return inventoryOperationRepository.findByStore_Id(storeId);
    }

    @GetMapping("stores/{id}/lots")
    public List<Lot> getStoreLots(@PathVariable(value = "id") Long storeId) {
        return lotRepository.findByStore_Id(storeId);
    }
}
