package com.dp1mtel.backend.controller

import com.dp1mtel.algo.ga.GASolver
import com.dp1mtel.algo.model.Customer
import com.dp1mtel.algo.model.Depot
import com.dp1mtel.algo.model.Instance
import com.dp1mtel.algo.solver.Solution
import com.dp1mtel.algo.tdFactories.TDFunctionFactories
import com.dp1mtel.backend.controller.graph.Graph
import com.dp1mtel.backend.exception.ResourceNotFoundException
import com.dp1mtel.backend.model.*
import com.dp1mtel.backend.repository.*
import com.dp1mtel.backend.requests.DispatchParameters
import com.dp1mtel.backend.requests.DispatchRequest
import com.dp1mtel.backend.util.*
import com.graphhopper.util.shapes.GHPoint
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.awt.geom.Point2D
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/distribution")
class DistributionController {
    // Autowire route and dispatch repositories

    @Autowired
    private lateinit var userRepository: UserRepository
    @Autowired
    private lateinit var storeRepository: StoreRepository
    @Autowired
    private lateinit var customerRepository: CustomerRepository
    @Autowired
    private lateinit var routeRepository: RouteRepository
    @Autowired
    private lateinit var orderStatusRepository: OrderStatusRepository
    @Autowired
    private lateinit var orderRepository: OrderRepository
    @Autowired
    private lateinit var graph: Graph

    private val pucpLocation = GHPoint(-12.0652157, -77.0817718)

    @PostMapping
    fun createDispatch(@Valid @RequestBody dispatch: Dispatch): Dispatch {
        val completedOrderStatus = orderStatusRepository.findBySlugEquals(OrderStatus.OrderStatusEnum.COMPLETED.toString())
                .orElseThrow { ResourceNotFoundException("OrderStatus", "slug", OrderStatus.OrderStatusEnum.COMPLETED) }
        // Update order status to COMPLETED
        dispatch.routes
                // Get all orders from routes
                .map { it.orders }
                .flatten()
                // Get them from database
                .map {it.id }
                .let { orderRepository.findAllByIdIn(it) }
                // Update status
                .map {
                    it.orderStatus = completedOrderStatus
                    it
                }
                .let { orderRepository.saveAll(it) }
        return Dispatch(routeRepository.saveAll(dispatch.routes.toMutableList()))
    }

    @PostMapping("/dispatch")
    fun dispatchOrders(@RequestBody request: DispatchRequest): List<Route> {
        val store = storeRepository.findById(request.storeId)
                .orElseThrow { ResourceNotFoundException("Store", "id", request.storeId) }
        val routes = runAlgorithm(DispatchParameters(request.availableVehicles, store, request.orderList))
        return routes
    }

    @GetMapping
    fun dispatchIndex(@RequestParam storeId: Long? = null): List<Route> {
        if (storeId == null) {
            return routeRepository.findAll()
        }
        val store = storeRepository.findById(storeId)
                .orElseThrow { ResourceNotFoundException("Store", "id", storeId) }
        return routeRepository.findAllByAssignedDriver_Store(store)
    }

//    @GetMapping("/dispatch/demo")
//    fun dispatchDemo(): List<Route> {
//        return runAlgorithm()
//    }

//    private fun runAlgorithm(): List<Route> {
//        val problem = Instances.getInstanceByName("050_C101")
//        if (!problem.isPresent) {
//            System.err.println("No problem")
//            return ArrayList()
//        }
//        val instance = problem.get()
//        val orderList = ArrayList<Order>()
//        val user = userRepository.findAll()[0]
//        val orderStatus = OrderStatus()
//        orderStatus.id = 1L
//        orderStatus.name = "nombre"
//        orderStatus.description = "descrip"
//
//        val customers = customerRepository.findAll()
//        for (i in 0..49) {
//            val buyer = customers[i]
//
//            val customer = instance.customers[i]
//            val address = UserAddress()
//            address.id = i + 1L
//            address.name = "Casa"
//            address.address = "Jr Casa 1"
//            address.coordinates = customer.position
//            val order = Order()
//            order.id = i + 1L
//            order.seller = user
//            order.customer = buyer
//            order.orderStatus = orderStatus
//            order.orderDetails = emptyList()
//            order.volume = customer.demand
//            order.userReadyTime = customer.readyTime
//            order.userDueTime = customer.dueTime
//            order.userServiceTime = customer.serviceTime
//            order.deliveryAddress = address
//            orderList.add(order)
//        }
//        val stores = storeRepository.findAll()
//        val store = stores[0]
//        store.coordinates = instance.depot.position
//        store.closingTime = instance.depot.closingTime
//        store.numVehicles = instance.availableVehicles
//        store.vehiclesCapacity = instance.vehicleCapacity
//        val request = DispatchParameters(store.numVehicles, store, orderList)
//        return runAlgorithm(request)
//    }

    private fun convertOrdersToInstance(params: DispatchParameters): Instance {
        val depot = Depot(params.store.coordinates.toPoint2D(), params.store.closingTime)
        val customers = ArrayList<Customer>()
        val orders = params.orderList
        for (i in orders.indices) {
            val order = orders[i]
            customers.add(
                    Customer(i + 1,
                            order.coordinates.point, order.volume, order.userReadyTime,
                            order.userDueTime, order.userServiceTime
                    )
            )
        }
        return object : Instance {
            override fun getName(): String {
                return ""
            }

            override fun getDepot(): Depot {
                return depot
            }

            override fun getCustomers(): List<Customer> {
                return customers
            }

            override fun getAvailableVehicles(): Int {
                return params.availableVehicles!!
            }

            override fun getVehicleCapacity(): Int {
                return params.store.vehiclesCapacity!!
            }

            override fun distanceMatrix(): Array<DoubleArray> {
                val n = getCustomers().size + 1
                val matrix = Array(n) { DoubleArray(n) }
                getCustomers().forEach { cA ->
                    getCustomers().forEach { cB ->
                        matrix[cA.number][cB.number] = cA.position.toGHPoint().distance(cB.position.toGHPoint())
                    }
                }
                matrix[getDepot().number][getDepot().number] = 0.0
                getCustomers().forEach { c ->
                    val distance = getDepot().position.toGHPoint().distance(c.position.toGHPoint())
                    matrix[getDepot().number][c.number] = distance
                    matrix[c.number][getDepot().number] = distance
                }
                return matrix
            }
        }
    }

    private fun runAlgorithm(params: DispatchParameters): List<Route> {
        val instance = convertOrdersToInstance(params)
        val solver = GASolver()
        val tdFunction = TDFunctionFactories.getFactoryByName("DEFAULT").get()
                .createTDFunction(instance)
        val solution = when (instance.customers.size) {
            0 -> null
            1 -> Solution(instance, tdFunction, listOf(com.dp1mtel.algo.solver.Route(listOf(instance.customers.first()))))
            else -> solver.solve(instance, tdFunction).orElse(null)
        }
        if (solution == null || !solution.isValid) {
            System.err.println("No solution")
            return ArrayList()
        }
        val routes = transformSolution(solution, params.orderList)
        return routes
    }

    private fun transformSolution(solution: Solution, orderList: List<Order>): List<Route> {
        val routes = mutableListOf<Route>()
        var i = 1L
        val depotCoords = solution.instance.depot.position.toGHPoint()
        for (route in solution.routes) {
            val routeOrders = route.customers.map { orderList[it.number - 1] }
            val r = Route()
            r.id = i++
            r.orders = routeOrders
            addSerializedRoute(r, depotCoords)
            routes.add(r)
        }
        return routes
    }

    private fun addSerializedRoute(route: Route, store: GHPoint) {
        val chunks = chunkRoute(route.orders, store)
        route.nodes = linkedSetOf<Point>()
        var index = 0
        chunks.forEach { chunk ->
            val path = graph.route(*chunk.toTypedArray())
            route.nodes.addAll(path.get().points.map { point ->
                Point((index++).toLong(), point.lat, point.lon)
            })
        }
    }

    private fun chunkRoute(orders: List<Order>, store: GHPoint): List<List<GHPoint>> {
        val orderCoords = orders.map {
            it.coordinates.toGHPoint()
        }.toMutableList().apply {
            add(0, store)
            add(store)
        }
        val chunks = orderCoords.chunked(2, Graph.MAX_CUSTOMERS_PER_ROUTE)
        if (chunks.size == 1) {
            return chunks
        }
        chunks.mapIndexed { index, list ->
            when (index == chunks.size - 1) {
                true -> list
                false -> {
                    list.toMutableList().apply {
                        val nextList = chunks[index + 1]
                        add(nextList.first())
                    }
                }
            }
        }
        return chunks
    }

    @GetMapping("/geocode")
    fun geocodeAddress(@RequestParam("address") address: String,
                       @RequestParam("limit") limit: Int? = 5): List<Point2D> {
        val suggestedCoordinates = graph.geocode(address, limit =  limit ?: 5)
                .map {
                    Point2D.Double(it.point.lat, it.point.lng)
                }
        return suggestedCoordinates
    }
}
