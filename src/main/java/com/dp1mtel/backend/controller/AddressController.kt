package com.dp1mtel.backend.controller

import com.dp1mtel.backend.model.UserAddress
import com.dp1mtel.backend.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/addresses")
class AddressController {
    @Autowired
    private lateinit var addressRepository: AddressRepository

    @GetMapping
    fun index(): List<UserAddress> {
        return addressRepository.findAll()
    }
}