package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceConflictException;
import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.Item;
import com.dp1mtel.backend.model.ProductCategory;
import com.dp1mtel.backend.model.ProductItem;
import com.dp1mtel.backend.repository.ItemRepository;
import com.dp1mtel.backend.repository.ProductItemRepository;
import com.dp1mtel.backend.repository.ProductRepository;
import com.dp1mtel.backend.responses.MassiveStorageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ItemController {

    @Autowired
    ItemRepository itemRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductItemRepository productItemRepository;

    @GetMapping("/items")
    //TODO: temporal fix, this shouldn't exist
    public List<Item> getAllItems(@Param(value = "name") String name) {
        if (name == null)
            return itemRepository.findAll().stream().filter( item -> {return item.getActive();}).collect(Collectors.toList());
        else
            return itemRepository.findByNameEquals(name).stream().filter( item -> {return item.getActive();}).collect(Collectors.toList());
    }

    @PostMapping("/items")
    public Item createItem(@Valid @RequestBody Item item) {
        Optional<Item> itemOptional = itemRepository.findById(item.getId());
        if (itemOptional.isPresent())
            throw new ResourceConflictException("Item",item.getId());
        return itemRepository.save(item);
    }

    @PatchMapping("/items/{id}")
    public ResponseEntity<?> disableItem(@PathVariable(value = "id") String itemId,
                                         @Valid @RequestBody Item itemToUpdate) {
        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));
        List<ProductItem> productItemList = productItemRepository.findByPrimaryKey_Item_Id_AndPrimaryKey_Product_ActiveTrue(item.getId());
        if (productItemList.size() > 0)
            return ResponseEntity.badRequest().build();
        itemToUpdate.setActive(false);
        itemRepository.save(itemToUpdate);
        return ResponseEntity.ok(itemToUpdate);
    }


    @GetMapping("/items/{id}")
    public Item getItemById(@PathVariable(value = "id") String itemId) {
        return itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));
    }

    @PutMapping("/items/{id}")
    public Item updateItem(@PathVariable(value = "id") String itemId,
                           @Valid @RequestBody Item itemDetails) {
        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));

        item.setName(itemDetails.getName());
        item.setDescription(itemDetails.getDescription());
        item.setExpires(itemDetails.getExpires());
        item.setVolume(itemDetails.getVolume());

        Item updatedItem = itemRepository.save(item);
        return updatedItem;
    }

    @PostMapping("/items/bulk")
    public ResponseEntity<MassiveStorageResponse> bulkSave(@RequestBody List<Item> items) {
        int correctlyInserted = 0;
        int badlyInserted = 0;
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            Item it = items.get(i);
            try {
                Optional<Item> itemOptional = itemRepository.findById(it.getId());
                if (!itemOptional.isPresent()) {
                    itemRepository.save(it);
                    correctlyInserted++;
                } else {
                    badlyInserted++;
                    positions.add(i);
                }
            } catch (Exception ex) {
                badlyInserted++;
                positions.add(i);
            }
        }
        return ResponseEntity.ok(new MassiveStorageResponse(String.valueOf(correctlyInserted),
                String.valueOf(badlyInserted),positions));
    }

    @DeleteMapping("/items/{id}")
    public ResponseEntity<String> deleteItem(@PathVariable(value = "id") String itemId) {
        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", itemId));

        try {
            itemRepository.delete(item);
        } catch (Exception e){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
