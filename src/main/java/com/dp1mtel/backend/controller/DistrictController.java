package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.model.District;
import com.dp1mtel.backend.repository.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/districts")
public class DistrictController {

    @Autowired
    DistrictRepository districtRepository;

    @GetMapping("")
    public List<District> getAllDistricts() { return districtRepository.findAll(); }
}
