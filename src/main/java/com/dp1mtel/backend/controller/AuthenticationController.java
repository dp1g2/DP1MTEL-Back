package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.AuthenticationException;
import com.dp1mtel.backend.model.Customer;
import com.dp1mtel.backend.model.User;
import com.dp1mtel.backend.repository.CustomerRepository;
import com.dp1mtel.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.parser.Entity;
import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/api")
public class AuthenticationController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    CustomerRepository customerRepository;

    @PostMapping("/login")
    public ResponseEntity<?> login(@Param("user") String user, @Param("password") String password, @Param("phone") String phone) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if ( user != null ) {
            System.out.println("User login: " + user);
            User loggedUser = userRepository.findByDni(user)
                    .orElseThrow(() -> new AuthenticationException(user,password));
            if (loggedUser.comparePassword(password))
                return ResponseEntity.ok(loggedUser);
            else
                return ResponseEntity.badRequest().build();

        } else if ( phone != null ) {
            System.out.println("Phone login: " + phone);
            Customer loggedCustomer = customerRepository.findByPasswordAndPhone(password,phone)
                    .orElseThrow(() -> new AuthenticationException(phone,password));
            return ResponseEntity.ok(loggedCustomer);
        }
        return ResponseEntity.badRequest().build();
    }
}
