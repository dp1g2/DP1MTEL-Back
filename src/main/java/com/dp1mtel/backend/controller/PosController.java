package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.*;
import com.dp1mtel.backend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class PosController {

    @Autowired
    LotRepository lotRepository;
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ComboRepository comboRepository;
    @Autowired
    StoreRepository storeRepository;
    @Autowired
    ProductCategoryDiscountRepository categoryDiscountRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    QuantityDiscountRepository quantityDiscountRepository;

    private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

    @GetMapping("/stores/{id}/pos/combos")
    public List<PosCombo> getPosCombos(@PathVariable(value = "id") Long storeId) {
        List<PosItem> posItemList = getStorePosItems(storeId, true);

        List<Combo> comboList = comboRepository.findAll().stream().filter(combo -> {
            return combo.getActive();
        }).collect(Collectors.toList());

        List<PosCombo> posComboList = new ArrayList<>();

        //comboListDiscount
        for (Combo combo : comboList) {
            List<Integer> integerList = new ArrayList<>();
            Double originalPrice = 0.0;
            for (ProductCombo productCombo : combo.getProductCombos()) {
                integerList.add(getProductStock(productCombo.getProduct(), posItemList) / productCombo.getQuantity());
                originalPrice = originalPrice + productCombo.getProduct().getPrice() * productCombo.getQuantity();
            }
            PosCombo newPosCombo = new PosCombo();
            newPosCombo.setCombo(combo);
            newPosCombo.setStock(Collections.min(integerList));
            newPosCombo.setDiscount(0.0);
            posComboList.add(newPosCombo);
        }

        return posComboList;
    }

    @GetMapping("/stores/{id}/pos/products")
    public List<PosProduct> getPosProducts(@PathVariable(value = "id") Long storeId) throws ParseException {
        List<PosItem> posItemList = getStorePosItems(storeId, true);

        List<Product> productList = productRepository.findAll().stream().filter(product -> {
            return product.getActive();
        }).collect(Collectors.toList());

        List<PosProduct> posProductList = new ArrayList<>();

        List<ProductCategoryDiscount> categoryDiscounts = categoryDiscountRepository.findAll();

        for (Product product : productList) {
            PosProduct newPosProduct = new PosProduct();
            newPosProduct.setProduct(product);

            Double discount = 0.0;

            for (ProductCategoryDiscount productCategoryDiscount : categoryDiscounts) {
                Date currentDate = new Date();
                currentDate = fmt.parse(fmt.format(currentDate));
                if (product.getProductCategory().getId().equals(productCategoryDiscount.getProductCategory().getId())){

                    if ((currentDate.after(productCategoryDiscount.getStartDate()) && currentDate.before(productCategoryDiscount.getEndDate())) ||
                            (currentDate.compareTo(productCategoryDiscount.getStartDate()) == 0) ||
                            (currentDate.compareTo(productCategoryDiscount.getEndDate()) == 0 ))
                        discount = Math.round(productCategoryDiscount.getDiscount() * 100.0) / 100.0;
                }

            }
            newPosProduct.setStock(getProductStock(product, posItemList));
            newPosProduct.setDiscount(discount);
            posProductList.add(newPosProduct);
        }
        return posProductList;
    }

    private Integer posItemIndex(Item item, List<PosItem> posItemList) {
        for (PosItem posItem : posItemList) {
            if (posItem.getItem() == item)
                return posItemList.indexOf(posItem);
        }
        return -1;
    }

    private Integer getProductStock(Product product, List<PosItem> posItemList) {
        List<Integer> integerList = new ArrayList<>();
        for (ProductItem productItem : product.getProductItems()) {
            Integer index = posItemIndex(productItem.getItem(), posItemList);
            if (index >= 0)
                integerList.add(posItemList.get(index).getStock() / productItem.getQuantity());
            else
                integerList.add(0);
        }

        return Collections.min(integerList);
    }

    @GetMapping("/stores/{id}/pos/items")
    public List<PosItem> getPosItems(@Param(value = "option") String option, @PathVariable(value = "id") Long storeId) {
        if (option == null || option.equals("physical"))
            return getStorePosItems(storeId, false);
        else
            return getStorePosItems(storeId, true);
    }

    private List<PosItem> getStorePosItems(Long storeId, Boolean logical) {
        Store store = storeRepository.findById(storeId)
                .orElseThrow(() -> new ResourceNotFoundException("Store", "id", storeId));

        List<Lot> lotList = lotRepository.findByStore_Id(storeId);

        List<PosItem> posItemList = new ArrayList<>();

        Boolean isInList;
        for (Lot lot : lotList) {
            if (lot.getItem().getActive()) {
                isInList = false;
                for (PosItem posItem : posItemList)
                    if (lot.getItem() == posItem.getItem()) {
                        isInList = true;
                        posItem.setStock(posItem.getStock() + lot.getQuantity());
                    }
                if (!isInList) {
                    PosItem newPosItem = new PosItem();
                    newPosItem.setItem(lot.getItem());
                    newPosItem.setStock(lot.getQuantity());
                    posItemList.add(newPosItem);
                }
            }
        }

        if (logical) {
            List<PosItem> reservedItems = getItemsFromReservedOrders(storeId);
            for (PosItem posItem : reservedItems) {
                Integer index = posItemIndex(posItem.getItem(), posItemList);
                posItemList.get(index)
                        .setStock(posItemList.get(index).getStock() - posItem.getStock());
            }

        }
        return posItemList;

    }

    private List<PosItem> getItemsFromReservedOrders(Long storeId) {
        Store store = storeRepository.findById(storeId)
                .orElseThrow(() -> new ResourceNotFoundException("Store", "id", storeId));

        List<Order> reservedOrders = orderRepository.findByStore_IdAndOrderStatus_SlugEquals(storeId, OrderStatus.OrderStatusEnum.RESERVED.toString());

        List<PosItem> reservedItems = new ArrayList<>();

        for (Order order : reservedOrders) {
            for (OrderDetail orderDetail : order.getOrderDetails()) {
                if (orderDetail.getProduct() != null) {
                    reservedItems.addAll(getReservedItemsFromProductDetail(orderDetail.getProduct(), orderDetail.getQuantity()));
                } else if (orderDetail.getCombo() != null) {
                    for (ProductCombo productCombo : orderDetail.getCombo().getProductCombos())
                        reservedItems.addAll(getReservedItemsFromProductDetail(productCombo.getProduct(), productCombo.getQuantity() * orderDetail.getQuantity()));
                }
            }
        }
        return reservedItems;
    }

    private List<PosItem> getReservedItemsFromProductDetail(Product product, Integer quantity) {
        List<PosItem> reservedItems = new ArrayList<>();
        for (ProductItem productItem : product.getProductItems()) {
            PosItem posItem = new PosItem();
            posItem.setItem(productItem.getItem());
            posItem.setStock(productItem.getQuantity() * quantity);
            reservedItems.add(posItem);
        }
        return reservedItems;
    }

    @GetMapping("/pos/products")
    public List<PosProduct> getDefaultPosProducts() throws ParseException {
        Store store = storeRepository.findAll().get(0);
        return getPosProducts(store.getId());
    }

    @GetMapping("/pos/combos")
    public List<PosCombo> getDefaultPosCombos() {
        Store store = storeRepository.findAll().get(0);
        return getPosCombos(store.getId());
    }

    @GetMapping("/pos/products/{id}")
    public PosProduct getPosProduct(@PathVariable("id") String productId) throws ParseException {
        return getDefaultPosProducts().stream().filter(p -> p.getProduct().getId().equals(productId))
                .findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("PosProduct", "id", productId));
    }

    @GetMapping("/pos/combos/{id}")
    public PosCombo getPosCombo(@PathVariable("id") String comboId) {
        return getDefaultPosCombos().stream().filter(p -> p.getCombo().getId().equals(comboId))
                .findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("PosCombo", "id", comboId));
    }
}
