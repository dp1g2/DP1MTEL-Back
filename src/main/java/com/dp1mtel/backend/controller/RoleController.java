package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceConflictException;
import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.*;
import com.dp1mtel.backend.repository.PermissionRepository;
import com.dp1mtel.backend.repository.RoleRepository;
import com.dp1mtel.backend.responses.MassiveStorageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RoleController {

    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PermissionRepository permissionRepository;

    @GetMapping("/roles")
    public List<Role> getAllRoles() { return roleRepository.findAll(); }

    @PostMapping("/roles")
    public Role createRole(@Valid @RequestBody Role role) { return roleRepository.save(role);}

    @GetMapping("/roles/{id}")
    public Role getRoleById(@PathVariable(value = "id") Long roleId) {
        return roleRepository.findById(roleId)
                .orElseThrow(() -> new ResourceNotFoundException("Role","id",roleId));
    }

    @PutMapping("/roles/{id}")
    public Role updateRole(@PathVariable(value = "id") Long roleId,
                           @Valid @RequestBody Role roleDetails) {
        Role role = roleRepository.findById(roleId)
                .orElseThrow(() -> new ResourceNotFoundException("Role","id",roleId));
        role.setDescription(roleDetails.getDescription());
        role.setName(roleDetails.getName());
        List<Permission> permissionList = new ArrayList<>(role.getPermissions());
        for (Permission permission : permissionList) {
            role.removePermission(permission);
        }
        for (Permission permission :
                roleDetails.getPermissions()
                ) {
            Permission newPermission = permissionRepository.findById(permission.getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Permission","id",permission.getId()));
            role.addPermission(newPermission);

        }

        Role updatedRole = roleRepository.save(role);
        return updatedRole;
    }

    @PostMapping("/roles/bulk")
    public ResponseEntity<MassiveStorageResponse> bulkSave(@RequestBody List<Role> roles) {
        int correctlyInserted = 0;
        int badlyInserted = 0;
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < roles.size(); i++) {
            Role role = roles.get(i);
            try {
                Optional<Role> roleOptional = roleRepository.findById(role.getId());

                if (roleOptional.isPresent())
                    throw new ResourceConflictException("Role", role.getId());

                Role newRole = new Role();
                newRole.setName(role.getName());
                newRole.setDescription(role.getDescription());

                for (Permission permission : role.getPermissions()) {
                    Permission permissionFound = permissionRepository.findByNameEquals(permission.getName())
                            .orElseThrow(() -> new ResourceNotFoundException("Permission", "name", permission.getName()));
                    newRole.addPermission(permissionFound);
                }

                roleRepository.save(newRole);
                correctlyInserted++;

            } catch (Exception ex) {
                badlyInserted++;
                positions.add(i);
            }
        }
        return ResponseEntity.ok(new MassiveStorageResponse(String.valueOf(correctlyInserted),
                String.valueOf(badlyInserted), positions));
    }

    @DeleteMapping("/roles/{id}")
    public ResponseEntity<?> deleteRole(@PathVariable(value = "id") Long roleId) {
        Role role = roleRepository.findById(roleId)
                .orElseThrow(() -> new ResourceNotFoundException("Role","id", roleId));

        roleRepository.delete(role);

        return ResponseEntity.ok().build();
    }
}
