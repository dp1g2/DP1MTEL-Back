package com.dp1mtel.backend.controller;


import com.dp1mtel.backend.model.GeneralInformation;
import com.dp1mtel.backend.repository.GeneralInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/general-information")
public class GeneralInformationController {

    @Autowired
    GeneralInformationRepository generalInformationRepository;

    @GetMapping("")
    public GeneralInformation getGeneralInformation() {
        return generalInformationRepository.findAll().get(0);
    }
}
