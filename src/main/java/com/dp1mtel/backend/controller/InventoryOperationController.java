package com.dp1mtel.backend.controller;

import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.*;
import com.dp1mtel.backend.repository.*;
import com.dp1mtel.backend.responses.MassiveStorageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class InventoryOperationController {
    @Autowired
    InventoryOperationRepository inventoryOperationRepository;
    @Autowired
    InventoryOperationTypeRepository inventoryOperationTypeRepository;
    @Autowired
    LotRepository lotRepository;
    @Autowired
    ItemRepository itemRepository;
    @Autowired
    StoreRepository storeRepository;
    @Autowired
    UserRepository userRepository;

    @GetMapping("/inventory-operations")
    public List<InventoryOperation> getAllInventoryOperations(@Param(value = "store-id") Long storeId) {
        if (storeId != null)
            return inventoryOperationRepository.findByStore_Id(storeId);
        else
            return inventoryOperationRepository.findAll();
    }

    @GetMapping("/inventory-operations/{id}")
    public InventoryOperation getInventoryOperationById(@PathVariable(value = "id") Long inventoryOperationId) {
        return inventoryOperationRepository.findById(inventoryOperationId)
                .orElseThrow(() -> new ResourceNotFoundException("InventoryOperation", "id", inventoryOperationId));
    }

    @PostMapping("/inventory-operations")
    public ResponseEntity<?> createInventoryOperation(@Valid @RequestBody InventoryOperation inventoryOperation) {
        if (inventoryOperation.getInventoryOperationType().getId() != null) {

            InventoryOperationType inventoryOperationType = inventoryOperationTypeRepository.findById(inventoryOperation.getInventoryOperationType().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("InventoryOperationType","id",inventoryOperation.getInventoryOperationType().getId()));
            Item item = itemRepository.findById(inventoryOperation.getItem().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Item","id",inventoryOperation.getItem().getId()));
            Store store = storeRepository.findById(inventoryOperation.getStore().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("Store","id",inventoryOperation.getStore().getId()));
            User user = userRepository.findById(inventoryOperation.getUser().getId())
                    .orElseThrow(() -> new ResourceNotFoundException("User","id",inventoryOperation.getUser().getId()));


            inventoryOperation.setInventoryOperationType(inventoryOperationType);

            if (!inventoryOperationType.getCreatesNewLot()) {
                Lot updatedLot = lotRepository.findById(inventoryOperation.getLot().getId())
                        .orElseThrow(() -> new ResourceNotFoundException("Lot","id",inventoryOperation.getLot().getId()));
                Integer newQuantity = updatedLot.getQuantity() + inventoryOperation.getQuantity()*inventoryOperationType.getMultiplier();
                if (newQuantity >= 0) {
                    updatedLot.setQuantity(newQuantity);
                    lotRepository.save(updatedLot);
                } else {
                    return ResponseEntity.badRequest().build();
                }
                inventoryOperation.setLot(updatedLot);

            } else {

                Lot newLot = new Lot();
                newLot.setQuantity(inventoryOperation.getQuantity());
                newLot.setItem(item);
                newLot.setStore(store);
                if ( item.getExpires() )
                    newLot.setExpirationDate(inventoryOperation.getLot().getExpirationDate());
                lotRepository.save(newLot);
                inventoryOperation.setLot(newLot);

            }


            inventoryOperation.setItem(item);
            inventoryOperation.setInventoryOperationType(inventoryOperationType);
            inventoryOperation.setStore(store);
            inventoryOperation.setUser(user);
            return ResponseEntity.ok(inventoryOperationRepository.save(inventoryOperation));
        } else
            return ResponseEntity.badRequest().build();

    }

    @PostMapping("/inventory-operations/bulk")
    public ResponseEntity<MassiveStorageResponse> bulkSave(@RequestBody List<InventoryOperation> operations) {
        int correctlyInserted = 0;
        int badlyInserted = 0;
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < operations.size(); i++) {
            InventoryOperation operation = operations.get(i);
            try {

                InventoryOperationType inventoryOperationType = inventoryOperationTypeRepository.findById(operation.getInventoryOperationType().getId())
                        .orElseThrow(() -> new ResourceNotFoundException("InventoryOperationType","id",operation.getInventoryOperationType().getId()));
                Item item = itemRepository.findById(operation.getItem().getId())
                        .orElseThrow(() -> new ResourceNotFoundException("Item","id",operation.getItem().getId()));
                Store store = storeRepository.findByDistrict(operation.getStore().getDistrict())
                        .orElseThrow(() -> new ResourceNotFoundException("Store","district",operation.getStore().getDistrict()));
                User user = userRepository.findByDni(operation.getUser().getDni())
                        .orElseThrow(() -> new ResourceNotFoundException("User","dni",operation.getUser().getDni()));


                operation.setInventoryOperationType(inventoryOperationType);

                if (!inventoryOperationType.getCreatesNewLot()) {
                    Lot updatedLot = lotRepository.findById(operation.getLot().getId())
                            .orElseThrow(() -> new ResourceNotFoundException("Lot","id",operation.getLot().getId()));
                    Integer newQuantity = updatedLot.getQuantity() + operation.getQuantity()*inventoryOperationType.getMultiplier();
                    if (newQuantity > 0) {
                        updatedLot.setQuantity(newQuantity);
                        lotRepository.save(updatedLot);
                    } else {
                        return ResponseEntity.badRequest().build();
                    }
                    operation.setLot(updatedLot);

                } else {

                    Lot newLot = new Lot();
                    newLot.setQuantity(operation.getQuantity());
                    newLot.setItem(item);
                    newLot.setStore(store);
                    if ( item.getExpires() )
                        newLot.setExpirationDate(operation.getLot().getExpirationDate());
                    lotRepository.save(newLot);
                    operation.setLot(newLot);

                }


                operation.setItem(item);
                operation.setInventoryOperationType(inventoryOperationType);
                operation.setStore(store);
                operation.setUser(user);
                inventoryOperationRepository.save(operation);
                correctlyInserted++;

            } catch (Exception ex) {
                System.out.println("INVENTORY, SE CAYO PORQUE -  " + ex.getMessage());
                ex.printStackTrace();
                badlyInserted++;
                positions.add(i);
            }
        }
        return ResponseEntity.ok(new MassiveStorageResponse(String.valueOf(correctlyInserted),
                String.valueOf(badlyInserted), positions));
    }


    @PutMapping("/inventory-operations/{id}")
    public InventoryOperation updateInventoryOperation(@PathVariable(value = "id") Long inventoryOperationId,
                                                       @Valid @RequestBody InventoryOperation inventoryOperationDetails) {
        InventoryOperation inventoryOperation = inventoryOperationRepository.findById(inventoryOperationId)
                .orElseThrow(() -> new ResourceNotFoundException("InventoryOperation","id",inventoryOperationId));
        inventoryOperation.setDescription(inventoryOperationDetails.getDescription());
        inventoryOperation.setItem(inventoryOperation.getItem());
        inventoryOperation.setQuantity(inventoryOperation.getQuantity());
        inventoryOperation.setStore(inventoryOperation.getStore());

        InventoryOperation updatedInventoryOperation = inventoryOperationRepository.save(inventoryOperation);
        return inventoryOperation;
    }

    @DeleteMapping("/inventory-operations/{id}")
    public ResponseEntity<?> deleteInventoryOperation(@PathVariable(value = "id") Long inventoryOperationId) {
        InventoryOperation inventoryOperation = inventoryOperationRepository.findById(inventoryOperationId)
                .orElseThrow(() -> new ResourceNotFoundException("InventoryOperation","id",inventoryOperationId));
        inventoryOperationRepository.delete(inventoryOperation);
        return ResponseEntity.ok().build();
    }
}
