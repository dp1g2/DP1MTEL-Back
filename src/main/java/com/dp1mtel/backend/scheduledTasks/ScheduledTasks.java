package com.dp1mtel.backend.scheduledTasks;

import com.dp1mtel.backend.tasks.Tasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ScheduledTasks {

    @Autowired
    Tasks tasks;

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    @Scheduled(initialDelay = 30000,fixedDelay = 1000*60*5)
    public void cancelReservedOrders() {
        tasks.updateOrdersStatus();
        tasks.updateCombosStatus();
    }

}
