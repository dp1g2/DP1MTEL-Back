package com.dp1mtel.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class AuthenticationException extends RuntimeException{
    public AuthenticationException( String user,String password) {
        super(String.format("Wrong user: %s or password: %s",user,password));
    }
}
