package com.dp1mtel.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.ACCEPTED)
public class BadRequestException extends RuntimeException{
    public BadRequestException(){
    }

    public BadRequestException(String message) {
        super(message);
    }
}
