package com.dp1mtel.backend.tasks;

import com.dp1mtel.backend.model.Combo;
import com.dp1mtel.backend.model.Order;
import com.dp1mtel.backend.model.OrderStatus;
import com.dp1mtel.backend.model.QuantityDiscount;
import com.dp1mtel.backend.repository.ComboRepository;
import com.dp1mtel.backend.repository.OrderRepository;
import com.dp1mtel.backend.repository.OrderStatusRepository;
import com.dp1mtel.backend.repository.QuantityDiscountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class Tasks {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    OrderStatusRepository orderStatusRepository;

    @Autowired
    QuantityDiscountRepository quantityDiscountRepository;

    @Autowired
    ComboRepository comboRepository;

    public void updateOrdersStatus() {
        for(Order order : orderRepository.findByOrderStatus_Slug(
                OrderStatus.OrderStatusEnum.RESERVED.toString())) {
            if (order.getReservationTime() != null && order.getReservationTime().after(new Date())) {
                    order.setOrderStatus(orderStatusRepository.findBySlugEquals(
                            OrderStatus.OrderStatusEnum.CANCELED.toString()).get());
                orderRepository.save(order);

            }
        }
    }

    public void updateCombosStatus() {
        List<QuantityDiscount> discountList = quantityDiscountRepository.findAll();
        for (QuantityDiscount discount : discountList) {
            if((new Date()).after(discount.getStartDate())){
                Combo combo = discount.getCombo();
                combo.setActive(true);
                comboRepository.save(combo);
            }

            if ((new Date()).after(discount.getEndDate())) {
                Combo combo = discount.getCombo();
                combo.setActive(false);
                comboRepository.save(combo);
            }
        }
    }
}
