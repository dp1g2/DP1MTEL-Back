package com.dp1mtel.backend;

import com.dp1mtel.backend.controller.graph.Graph;
import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.*;
import com.dp1mtel.backend.repository.*;
import com.dp1mtel.backend.scheduledTasks.ScheduledTasks;
import com.dp1mtel.backend.tasks.Tasks;
import com.dp1mtel.backend.util.Point;
import com.dp1mtel.backend.util.PointKt;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.PointList;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.awt.geom.Point2D;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@SpringBootApplication
//@EnableJpaAuditing
@EnableScheduling
public class BackendApplication {

	@Autowired
	UserRepository userRepository;
	@Autowired
	PermissionRepository permissionRepository;
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	InventoryOperationTypeRepository inventoryOperationTypeRepository;
	@Autowired
	DistrictRepository districtRepository;
	@Autowired
	GeneralInformationRepository generalInformationRepository;
	@Autowired
	Tasks tasks;


	@Autowired
	ProductCategoryRepository productCategoryRepository;
	@Autowired
	ProductRepository productRepository;
	@Autowired
	ItemRepository itemRepository;
	@Autowired
	ComboRepository comboRepository;
	@Autowired
	StoreRepository storeRepository;
	@Autowired
    CustomerRepository customerRepository;
	@Autowired
	AddressRepository addressRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    OrderStatusRepository orderStatusRepository;
    @Autowired
	BillRepository billRepository;

    private Graph graph = new Graph();

    public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void	initDataBase() throws UnsupportedEncodingException, NoSuchAlgorithmException {
    	Store store = new Store();
    	if (generalInformationRepository.findAll().size() == 0) {

    		Bill bill = new Bill();
    		bill.setCodeBoleta(0L);
    		bill.setCodeFactura(0L);
    		bill.setCodeNotaCredito(0L);
    		billRepository.save(bill);

    	    GeneralInformation generalInformation = new GeneralInformation();
    	    generalInformation.setIgv(0.18);
    	    generalInformation.setName("Margaritatel S.A.");
    	    generalInformation.setRuc("10084525531");
    	    generalInformationRepository.save(generalInformation);

			for (OrderStatus.OrderStatusEnum status : OrderStatus.OrderStatusEnum.values()) {
				OrderStatus orderStatus = new OrderStatus();
				orderStatus.setSlug(status.toString());
				switch (status) {
					case COMPLETED:
						orderStatus.setName("Completado");
						orderStatus.setDescription("El pedido se ha completado");
						break;
					case PAID:
						orderStatus.setName("Pagado");
						orderStatus.setDescription("El pedido se pagó");
						break;
					case CANCELED:
						orderStatus.setName("Cancelado");
						orderStatus.setDescription("El pedido se canceló");
						break;
					case NON_COMPLETED:
						orderStatus.setName("No Completado");
						orderStatus.setDescription("El pedido se llego a completar");
						break;
                    case RESERVED:
                        orderStatus.setName("Reservado");
                        orderStatus.setDescription("El pedido esta reservado");
                        break;
                    case SENT:
                        orderStatus.setName("Enviado");
                        orderStatus.setDescription("El pedido se ha enviado");
                        break;
					case RETURNED:
						orderStatus.setName("Devuelto");
						orderStatus.setDescription("El pedido ha sido devuelto");
						break;
				}
				orderStatusRepository.save(orderStatus);
			}

			List<Permission> permissionList = new ArrayList<>();
			permissionList.add(new Permission("Ventas","Accede a todas las funcionales de Ventas"));
			permissionList.add(new Permission("Precios y Descuentos","Accede a todas las funcionales de Precios y Descuentos"));
			permissionList.add(new Permission("Almacen","Accede a todas las funcionales de Almacen"));
			permissionList.add(new Permission("Administracion","Accede a todas las funcionales de Administracion"));
			permissionList.add(new Permission("Reportes","Accede a todas las funcionales de Reportes"));
			permissionList.add(new Permission("Seguridad","Accede a todas las funcionales de Seguridad"));
			permissionList.add(new Permission("Distribucion","Accede a todas las funcionales de Distribucion"));
			permissionList.add(new Permission("Locales","Accede a todas las funcionales de Locales"));

			permissionRepository.saveAll(permissionList);

			Role role = new Role();
			role.setName("Admin");
			role.setDescription("Rol con todos los permisos");
			for (Permission permission : permissionList) {
				role.addPermission(permission);
			}
			role = roleRepository.save(role);

			List<InventoryOperationType> inventoryOperationTypeList = new ArrayList<>();
			inventoryOperationTypeList.add(new InventoryOperationType("Venta","Salida por venta",-1,false));
			inventoryOperationTypeList.add(new InventoryOperationType("Devolucion","Ingreso por devolucion",1,false));
			inventoryOperationTypeList.add(new InventoryOperationType("Compra","Ingreso por compra",1,true));
			inventoryOperationTypeList.add(new InventoryOperationType("Daño","Salida por daño",-1,false));
			inventoryOperationTypeRepository.saveAll(inventoryOperationTypeList);

			District districtDefault = new District("Default");
			districtDefault = districtRepository.save(districtDefault);

			List<District> districtList = new ArrayList<>();
			districtList.add(new District("Breña"));
			districtList.add(new District("La Victoria"));
			districtList.add(new District("Lima"));
			districtList.add(new District("Lince"));
			districtList.add(new District("Rimac"));
			districtList.add(new District("Barranco"));
			districtList.add(new District("Jesús María"));
			districtList.add(new District("La Molina"));
			districtList.add(new District("Magdalena"));
			districtList.add(new District("Miraflores"));
			districtList.add(new District("Pueblo Libre"));
			districtList.add(new District("San Borja"));
			districtList.add(new District("San Isidro"));
			districtList.add(new District("San Luis"));
			districtList.add(new District("San Miguel"));
			districtList.add(new District("Santiago de Surco"));
			districtList.add(new District("Surquillo"));
			districtList.add(new District("Ate"));
			districtList.add(new District("Cieneguilla"));
			districtList.add(new District("Chaclacayo"));
			districtList.add(new District("San Juan de Lurigancho"));
			districtList.add(new District("El Agustino"));
			districtList.add(new District("Lurigancho-Chosica"));
			districtList.add(new District("Santa Anita"));
			districtList.add(new District("Ancón"));
			districtList.add(new District("Carabayllo"));
			districtList.add(new District("Comas"));
			districtList.add(new District("Independencia"));
			districtList.add(new District("Los Olivos"));
			districtList.add(new District("Puente Piedra"));
			districtList.add(new District("San Martín de Porres"));
			districtList.add(new District("Santa Rosa"));
			districtList.add(new District("Chorrillos"));
			districtList.add(new District("Lurín"));
			districtList.add(new District("Pachacamac"));
			districtList.add(new District("Pucusana"));
			districtList.add(new District("Punta Hermosa"));
			districtList.add(new District("Punta Negra"));
			districtList.add(new District("San Bartolo"));
			districtList.add(new District("San Juan de Miraflores"));
			districtList.add(new District("Santa María del Mar"));
			districtList.add(new District("Villa el Salvador"));
			districtList.add(new District("Villa María del Triunfo"));
			districtRepository.saveAll(districtList);

			store = new Store();
			store.setNumVehicles(12);
			store.setAddress("Av. Default 123");
			store.setActive(true);
			store.setVehiclesCapacity(50);
			store.setDistrict("Lima");
			store.setNumEmployees(1);
			store.setCoordinates(new Point(0, -12.06654, -77.04848));
			storeRepository.save(store);

			User user = new User();
			user.setFirstName("Admin");
			user.setLastName("Admin");
			user.setBirthday(new Date());
			user.setPassword("admin");
			user.generatePassword();
			user.setDni("admin");
			user.setGender("?");
			user.setActive(true);
			user.setEmail("admin@margaritatel.com");
			user.setRole(role);
			user.setStore(store);
			userRepository.save(user);
		}

		tasks.updateOrdersStatus();

//        for (int i = 0; i < 50; i++) {
//            User user = new User();
//            user.setFirstName("User " + i);
//            user.setLastName("Admin" + i);
//            user.setBirthday(new Date());
//            user.setAddress("Direccion " + i);
//            user.setPassword("admin");
//            if (i == 0) {
//                user.setDni("admin");
//            } else {
//                user.setDni("admin" + i);
//            }
//            user.setGender("?");
//            user.setEmail("admin" + i + "@gmail.com");
//            user.setRole(role);
//            user.setStore(store);
//            userRepository.save(user);
//        }





/*

		ProductCategory productCategory = new ProductCategory();
		productCategory.setName("Chocolates");
		productCategory.setDescription("Son Choloates :'v");
		productCategory.setId("CAT0002");
		productCategory.setActive(true);
		productCategoryRepository.save(productCategory);

		productCategory = new ProductCategory();
		productCategory.setName("Flores");
		productCategory.setDescription("Unas Flores");
		productCategory.setId("CAT0001");
		productCategory.setActive(true);
		productCategory = productCategoryRepository.save(productCategory);

		Item item = new Item();
		item.setName("Rosa");
		item.setDescription("Una Rosa");
        item.setId("ITE0001");
        item.setExpires(true);
		item.setStock(100);
        item.setVolume(2.0);
        item.setActive(true);

		item = itemRepository.save(item);

		Product product = new Product();
		product.setName("Docena de rosas");
		product.setPrice(12.4);
		product.setDescription("12 rosas");
		product.setActive(true);
        product.setProductCategory(productCategory);
        product.setId("PRO0001");
        product.addItem(item,12);
		product = productRepository.save(product);

		Combo combo = new Combo();
		combo.setPrice(20.0);
		combo.setName("2 Docenas de rosas");
		combo.setDescription("Dos ramos que contienen 12 rosas cada uno");
		combo.setActive(true);
		combo.setId("COM0001");
		combo.addProduct(product,2);

		combo = comboRepository.save(combo);
        List<Point2D> addressCoordinates = Arrays.asList(
                new Point2D.Double(-12.07251, -77.03822),
                new Point2D.Double(-12.07693, -77.04474),
                new Point2D.Double(-12.0639, -77.0419),
                new Point2D.Double(-12.0645, -77.0411),
                new Point2D.Double(-12.0644, -77.0503),
                new Point2D.Double(-12.0722, -77.0493),
                new Point2D.Double(-12.0719, -77.0541),
                new Point2D.Double(-12.0684, -77.0458),
                new Point2D.Double(-12.067, -77.0374),
                new Point2D.Double(-12.0656, -77.0514)
        );
        List<UserAddress> addresses = new ArrayList<>();
        OrderStatus orderStatus = orderStatusRepository.findBySlugEquals(OrderStatus.OrderStatusEnum.PAID.toString())
                .orElseThrow(() -> new ResourceNotFoundException("OrderStatus","slug",OrderStatus.OrderStatusEnum.PAID.toString()));
		for (int i = 0; i < 10; i++) {
			Customer buyer = new Customer();
			buyer.setFirstName("User " + i);
			buyer.setLastName("Admin" + i);
			buyer.setDocument("Doc " + i);
			UserAddress address = new UserAddress();
			address.setName("Address " + i);
			address.setAddress("Address " + i);
			address.setCoordinates(PointKt.toPoint(addressCoordinates.get(i)));
			address = addressRepository.save(address);
			addresses.add(address);
			buyer.getAddresses().add(address);
			buyer.setPhone(new String(new char[9]).replace("\0", "10" + String.valueOf(i)).substring(0, 9));
			customerRepository.save(buyer);
		}
		//Creating clients for massive charge...
		List<Point2D> addressCoordinatesForMassive = Arrays.asList(
				new Point2D.Double(-12.07951, -77.03322),
				new Point2D.Double(-12.08093, -77.04274),
				new Point2D.Double(-12.06939, -77.0519),
				new Point2D.Double(-12.06945, -77.0311),
				new Point2D.Double(-12.0804, -77.0513),
				new Point2D.Double(-12.0782, -77.0483)
		);

		for (int i = 0; i < 6; i++) {
			Customer buyer = new Customer();
			buyer.setFirstName("Cliente" + i);
			buyer.setLastName("Apellido" + i);
			buyer.setDocument(32165498 -  i + "");
			UserAddress address = new UserAddress();
			address.setName("Address " + i);
			address.setAddress("Address " + i);
			address.setCoordinates(PointKt.toPoint(addressCoordinatesForMassive.get(i)));
			address = addressRepository.save(address);
			addresses.add(address);
			buyer.getAddresses().add(address);
			buyer.setPhone(new String(new char[9]).replace("\0", String.valueOf(i)).substring(0, 9));
			customerRepository.save(buyer);
		}

		List<Customer> customers = customerRepository.findAll();
		List<User> users = userRepository.findAll();
		for (int i = 0; i < 10; i++) {
		    Order order = new Order();
		    order.setSeller(users.get(0));
		    int customerIndex = ThreadLocalRandom.current().nextInt(customers.size());
		    Customer customer = customers.get(customerIndex);
		    order.setCustomer(customer);
		    order.setStore(users.get(0).getStore());
		    order.setOrderStatus(orderStatus);
		    OrderDetail detail = new OrderDetail();
		    boolean isProduct = Math.random() < 0.5;
		    if (isProduct) {
                detail.setProduct(product);
            } else {
		        detail.setCombo(combo);
            }
            detail.setQuantity(ThreadLocalRandom.current().nextInt(1, 5));
		    double subtotal = 0;
		    if (isProduct) {
		        subtotal = detail.getProduct().getPrice() * detail.getQuantity();
            } else {
                subtotal = detail.getCombo().getPrice() * detail.getQuantity();
            }
		    detail.setSubTotal(subtotal);
		    detail.setPrice(subtotal);
		    detail.setDiscount(0.0);
		    order.addOrderDetail(detail);
		    order.setDeliveryAddress(addresses.get(customerIndex));
		    int readyTime = ThreadLocalRandom.current().nextInt(10, 18);
		    order.setUserReadyTime(readyTime);
		    order.setUserServiceTime(0);
		    order.setUserDueTime(readyTime + 2);
		    orderRepository.save(order);
        }

        Role driverRole = new Role();
        driverRole.setName("Conductor");
        driverRole.setDescription("Conductores");
        driverRole = roleRepository.save(driverRole);

        Role sellerRole = new Role();
        sellerRole.setName("Vendedor");
        sellerRole.setDescription("Vendedores");
        sellerRole = roleRepository.save(sellerRole);

        for (int i = 0; i < 5; i++) {
            User driver = new User();
            driver.setFirstName("Ana María");
            driver.setLastName("Luengo " + i);
            driver.setDni("1234568" + Integer.toString(i));
            driver.setGender("Femenino");
            driver.setActive(true);
            driver.setEmail("anamal@margaritatel.com");
            driver.setPassword("1234");
            driver.generatePassword();
            driver.setRole(driverRole);
            driver.setStore(store);
            driver.setBirthday(new Date());
            userRepository.save(driver);
        }
        */
    }

	@EventListener(ApplicationReadyEvent.class)
	public void initGraph() {
        graph.init(Graph.MODE.API);
    }
}
