package com.dp1mtel.backend.requests;

import com.dp1mtel.backend.model.Order;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.util.List;

public class DispatchRequest {
    private int availableVehicles;
    private long storeId;
    private List<Order> orderList;

    public DispatchRequest() {}

    public DispatchRequest(int availableVehicles, long storeId, List<Order> orderList) {
        setAvailableVehicles(availableVehicles);
        setStoreId(storeId);
        setOrderList(orderList);
    }

    public int getAvailableVehicles() {
        return availableVehicles;
    }

    public void setAvailableVehicles(int availableVehicles) {
        this.availableVehicles = availableVehicles;
    }


    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }

    public long getStoreId() {
        return storeId;
    }

    public void setStoreId(long storeId) {
        this.storeId = storeId;
    }
}
