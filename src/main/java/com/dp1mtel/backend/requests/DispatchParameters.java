package com.dp1mtel.backend.requests;

import com.dp1mtel.backend.model.Order;
import com.dp1mtel.backend.model.Store;

import java.util.List;

public class DispatchParameters {
    private Integer availableVehicles;
    private Store store;
    private List<Order> orderList;

    public DispatchParameters(Integer availableVehicles, Store store, List<Order> orderList) {
        setAvailableVehicles(availableVehicles);
        setStore(store);
        setOrderList(orderList);
    }

    public Integer getAvailableVehicles() {
        return availableVehicles;
    }

    public void setAvailableVehicles(Integer availableVehicles) {
        this.availableVehicles = availableVehicles;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<Order> orderList) {
        this.orderList = orderList;
    }
}
