package com.dp1mtel.backend.util

import com.graphhopper.util.shapes.GHPoint
import java.awt.geom.Point2D
import kotlin.math.*

val EARTH_RADIUS = 6378 // km

fun GHPoint.addKilometers(dx: Double, dy: Double): GHPoint {
    val newLat = lat + (dy / EARTH_RADIUS) * 180 / PI
    val newLong = lon + (dx / EARTH_RADIUS) * 180 / PI / cos(lat * PI / 180)
    return GHPoint(newLat, newLong)
}

fun GHPoint.toPoint2D(): Point2D = Point2D.Double(lat, lon)

fun Point2D.toGHPoint(): GHPoint = GHPoint(x, y)

fun GHPoint.distance(dest: GHPoint): Double {
    val latDiff = Math.toRadians(lat - dest.lat)
    val lonDiff = Math.toRadians(lon - dest.lon)
    val a = sin(latDiff / 2).pow(2) +
            cos(Math.toRadians(lat)) * cos(Math.toRadians(dest.lat)) *
            sin(lonDiff / 2).pow(2)
    val c = 2 * atan2(sqrt(a), sqrt(1 - a))
    val distance = EARTH_RADIUS * c
    return distance
}