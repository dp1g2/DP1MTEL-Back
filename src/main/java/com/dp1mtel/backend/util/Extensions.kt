package com.dp1mtel.backend.util

fun <T> Iterable<T>.chunked(minSize: Int, size: Int): List<List<T>> {
    val chunks = this.chunked(size)
    val mutChunks = chunks.map {
        it.toMutableList()
    }.toMutableList()
    if (mutChunks.last().size < minSize) {
        mutChunks[mutChunks.lastIndex - 1].addAll(mutChunks.last())
        mutChunks.removeAt(mutChunks.lastIndex)
    }
    return mutChunks
}