package com.dp1mtel.backend.util

import com.graphhopper.util.shapes.GHPoint
import java.awt.geom.Point2D

class Point {

    constructor(id: Long, x: Double, y: Double) {
        this.id = id
        this.x = x
        this.y = y
    }

    var id: Long = 0L
    @Transient val point = Point2D.Double(0.0, 0.0)
    var x: Double
        get() = point.x
        set(value) { point.x = value }
    var y: Double
        get() = point.y
        set(value) { point.y = value }

    fun toGHPoint(): GHPoint {
        return point.toGHPoint()
    }

    fun toPoint2D(): Point2D {
        return point
    }
}

fun Point2D.toPoint(): Point {
    return Point(0, x, y)
}