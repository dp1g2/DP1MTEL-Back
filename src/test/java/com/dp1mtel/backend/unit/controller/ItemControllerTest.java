package com.dp1mtel.backend.unit.controller;

import com.dp1mtel.backend.controller.ItemController;
import com.dp1mtel.backend.model.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class ItemControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ItemController itemController;

    @Test
    public void getItems() throws Exception{
        Item item = new Item();
        item.setName("Rosa");
        List<Item> allItems = Collections.singletonList(item);

        given(itemController.getAllItems("")).willReturn(allItems);

        mvc.perform(get("/api/items")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(item.getName())));
    }

    @Test
    public void getItemsById() throws Exception{
        Item item = new Item();
        item.setName("Rosa");
        item.setId(String.valueOf(1));

        given(itemController.getItemById(item.getId())).willReturn(item);

        mvc.perform(get("/api/items/" + item.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is(item.getName())));
    }

}
