package com.dp1mtel.backend.unit.controller;

import com.dp1mtel.backend.controller.UserController;
import com.dp1mtel.backend.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.hamcrest.core.Is.is;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserController userController;

    @Test
    public void getUsers() throws Exception{
        User user = new User();
        user.setPassword("123456");
        user.setDni("0123456");

        List<User> allUsers = Collections.singletonList(user);

        given(userController.getAllUsers()).willReturn(allUsers);

        mvc.perform(get("/api/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].dni", is(user.getDni())))
                .andExpect(jsonPath("$",hasSize(1)));
    }

    @Test
    public void getUserById() throws Exception{
        User user = new User();
        user.setPassword("123456");
        user.setDni("0123456");
        user.setId((long) 1);

        given(userController.getUserById(user.getId())).willReturn(user);

        mvc.perform(get("/api/users/" + user.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("dni", is(user.getDni())));
    }
}
