package com.dp1mtel.backend.unit.repository;

import com.dp1mtel.backend.exception.ResourceNotFoundException;
import com.dp1mtel.backend.model.Item;
import com.dp1mtel.backend.repository.ItemRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.List;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ItemRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void whenFindAll() {
        //given
        Item firstItem = new Item();
        firstItem.setId(String.valueOf(1));
        firstItem.setName("Rosa");
        firstItem.setCreatedAt(new Date());
        firstItem.setUpdatedAt(new Date());
        testEntityManager.persist(firstItem);
        testEntityManager.flush();

        Item secondItem = new Item();
        secondItem.setId(String.valueOf(2));
        secondItem.setName("Margarita");
        secondItem.setCreatedAt(new Date());
        secondItem.setUpdatedAt(new Date());
        testEntityManager.persist(secondItem);
        testEntityManager.flush();

        //when
        List<Item> items = itemRepository.findAll();

        //then
        assertThat(items.size()).isEqualTo(2);
        assertThat(items.get(1)).isEqualTo(firstItem);
        assertThat(items.get(2)).isEqualTo(secondItem);
    }

    @Test
    public void whenFindAllById() {
        //given
        Item item = new Item();
        item.setName("Rosa");
        testEntityManager.persist(item);
        testEntityManager.flush();

        //when
        Item testItem = itemRepository.findById(item.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Item", "id", item.getId()));;

        //then
        assertThat(testItem.getName()).isEqualTo(item.getName());
    }
}
